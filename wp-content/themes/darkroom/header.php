<?php
/*
* @ Header
*/
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1416373491739082'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1416373491739082&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( of_get_option('general_fav_icon') ) { ?>
	<link rel="shortcut icon" href="<?php echo of_get_option('general_fav_icon'); ?>" />
	<?php } ?>
	<?php
	wp_head();
	?>
</head>

<body <?php body_class(); ?>>
<?php
//Background overlays
do_action('mtheme_contextmenu_msg');
do_action('mtheme_background_overlays');
//Demo Panel if active
do_action('mtheme_demo_panel');
//Check for sidebar choice
do_action('mtheme_get_sidebar_choice');
//Backround display status
//Mobile menu
get_template_part('/includes/mobile','menu');

if ( !is_page_template('template-fullscreen-home.php') && !is_404() ) {
	get_template_part('/includes/background/background','display');
}
if ( is_page_template('template-fullscreen-home.php') && is_front_page() ) {
	$featured_page=mtheme_get_active_fullscreen_post();
	$custom = get_post_custom( $featured_page );
	if ( isSet($custom[ MTHEME . "_fullscreen_type"][0]) ) {
		$fullscreen_type = $custom[ MTHEME . "_fullscreen_type"][0];
	}
	if ($fullscreen_type=="photowall" || $fullscreen_type=="carousel" ) {
		get_template_part('/includes/background/background','display');
	}
}
//Header Navigation elements
get_template_part('header','navigation');

if ( is_singular() ) {
	$post_type = get_post_type( get_the_ID() );
	$custom = get_post_custom( get_the_ID() );
	if (isset($custom[MTHEME . '_pagestyle'][0])) { $mtheme_pagestyle=$custom[MTHEME . '_pagestyle'][0]; }
}

if ( !mtheme_is_fullscreen_post() ) {

	echo '<div class="container-outer">';
	
	echo '<div class="container-wrapper container-fullwidth">';

	echo '<div class="container clearfix">';
	//Pass if it's not a fullscreen
}
?>