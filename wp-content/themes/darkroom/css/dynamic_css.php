<?php
$dynamic_css='';
$theme_imagepath =  get_template_directory_uri() . '/images/';
//Background Overlay
$background_overlay=of_get_option('general_background_overlay');
if ( $background_overlay ) { 
	$dynamic_css .= '.pattern-overlay { background: transparent url('.$theme_imagepath. 'overlays/'.$background_overlay.'.png) repeat; } ';
} else { 
	$dynamic_css .= '.pattern-overlay {background:none;}'; 
}

$heading_classes='
.entry-title h1,
.entry-title h2,
h1,
h2,
h3,
h4,
h5,
h6,
.sidebar h3';

$page_heading_classes='
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6,
ul#portfolio-tiny h4,
ul#portfolio-small h4, ul#portfolio-large h4,
.entry-post-title h2
';

$menu_classes = '.mainmenu-navigation .homemenu ul li, .mainmenu-navigation .homemenu ul li a';
$slideshow_title_classes = '.slideshow_title, .static_slideshow_title';
//Font
if (of_get_option('default_googlewebfonts')) {
	$dynamic_css .= mtheme_apply_font ( "heading_font" , $heading_classes );
	$dynamic_css .= mtheme_apply_font ( "page_headings" , $page_heading_classes );
	$dynamic_css .= mtheme_apply_font ( "menu_font" , $menu_classes );
	$dynamic_css .= mtheme_apply_font ( "super_title" , $slideshow_title_classes );

	$super_title_style = of_get_option('super_title_style');
	if ($super_title_style) {
		$dynamic_css .= $slideshow_title_classes . ' { font-style:'.$super_title_style.';}';
	}
	$super_title_weight = of_get_option('super_title_weight');
	if ($super_title_weight) {
		$dynamic_css .= $slideshow_title_classes . ' { font-weight:'.$super_title_weight.';}';
	}

	$headings_font_style = of_get_option('headings_font_style');
	if ($headings_font_style) {
		$dynamic_css .= $heading_classes .' { font-style:'.$headings_font_style.';}';
	}
	$headings_font_weight = of_get_option('headings_font_weight');
	if ($headings_font_weight) {
		$dynamic_css .= $heading_classes .' { font-weight:'.$headings_font_weight.';}';
	}

	$contentheadings_font_style = of_get_option('contentheadings_font_style');
	if ($contentheadings_font_style) {
		$dynamic_css .= $page_heading_classes .' { font-style:'.$contentheadings_font_style.';}';
	}
	$contentheadings_font_weight = of_get_option('contentheadings_font_weight');
	if ($contentheadings_font_weight) {
		$dynamic_css .= $page_heading_classes .' { font-weight:'.$contentheadings_font_weight.';}';
	}

	$menu_font_style = of_get_option('menu_font_style');
	if ($menu_font_style) {
		$dynamic_css .= $menu_classes .' { font-style:'.$menu_font_style.';}';
	}
	$menu_font_weight = of_get_option('menu_font_weight');
	if ($menu_font_weight) {
		$dynamic_css .= $menu_classes .' { font-weight:'.$menu_font_weight.';}';
	}
}
//Logo
$logo_width=of_get_option('logo_width');
if ($logo_width) {
	$dynamic_css .= '.logo img { width: '.$logo_width.'px; }';
}
$logo_topmargin=of_get_option('logo_topmargin');
if ($logo_topmargin) {
	$dynamic_css .= '.logo { margin-top: '.$logo_topmargin.'px; }';
}
$logo_bottommargin=of_get_option('logo_bottommargin');
if ($logo_bottommargin) {
	$dynamic_css .= '.logo { margin-bottom: '.$logo_bottommargin.'px; }';
}
$logo_leftmargin=of_get_option('logo_leftmargin');
if ($logo_leftmargin) {
	$dynamic_css .= '.logo { margin-left: '.$logo_leftmargin.'px; }';
}
$responsive_logo_width = of_get_option('responsive_logo_width');
if ($responsive_logo_width) {
	$dynamic_css .= '.logo-mobile .logoimage { width: '.$responsive_logo_width.'px; }';
	$dynamic_css .= '.logo-mobile .logoimage { height: auto; }';
}
$responsive_logo_topmargin = of_get_option('responsive_logo_topmargin');
if ($responsive_logo_topmargin) {
	$dynamic_css .= '.logo-mobile .logoimage { top: '.$responsive_logo_topmargin.'px; }';
}

$background_opacity = of_get_option('background_opacity');
$background_opacity_percent = $background_opacity/100;
if ( isSet($background_opacity) )  {
	$dynamic_css .= '.pattern-overlay { opacity: '.$background_opacity_percent.'; }';
}

$header_background_color = of_get_option('header_background_color');
if ( $header_background_color )  {
	$dynamic_css .= 'body .header-block-wrap,.header-type-bright .header-block-wrap,.header-type-dark .header-block-wrap { background: '.$header_background_color.'; }';
	$dynamic_css .= 'body .header-block-wrap .social-toggle-wrap,.header-type-dark .social-toggle-wrap,.header-type-bright .social-toggle-wrap { top:90px; }';
}
$stickyheader_background_color = of_get_option('stickyheader_background_color');
if ( $stickyheader_background_color )  {
	$dynamic_css .= '.sticky-menu-activate .header-elements-wrap,.header-type-bright .sticky-menu-activate .header-elements-wrap,.header-type-dark .sticky-menu-activate .header-elements-wrap { background: '.$stickyheader_background_color.'; }';
}
//Accents
$accent_color=of_get_option('accent_color');
$accent_color_rgb=mtheme_hex2RGB($accent_color,true);
$slideshow_transbar_rgb=mtheme_hex2RGB($accent_color,true);

if ($accent_color) {
$accent_change_color = "
.entry-content a,
.header-search,
.toggle-shortcode-wrap .active,
.toggle-shortcode-wrap .toggle-shortcode:hover,
.project-details a,
.post-single-tags a:hover,
.post-meta-category a:hover,
.post-single-meta a:hover,
.post-navigation a:hover,
.entry-post-title h2 a:hover,
.comment-reply-title small a,
.entry-content .toggle-shortcode,
.header-shopping-cart a:hover,
#gridblock-filter-select i,
.entry-content .blogpost_readmore a,
.pricing-table .pricing_highlight .pricing-price,
.footer-widget .widget_nav_menu a:hover,
.project-details-link h4 a,
.entry-content .readmore_link a:hover,
.quote_say i,
.gridblock-element:hover h4 a,
.gridblock-list h4 a:hover,
.social-header-wrap ul li.contact-text a:hover,
.postsummarywrap a:hover,
.client-company a:hover,
.portfolio-share li a:hover,
.woocommerce ul.products li.product h3:hover,
.woocommerce-page ul.products li.product h3:hover,
.woocommerce .product_meta a:hover,
.min-search .fa-search:hover,
ul.gridblock-listbox .work-details h4 a:hover,
#gridblock-filters li .is-active,
#gridblock-filters li a:focus,
#gridblock-filters a:focus,
#gridblock-filters li .is-active,
#gridblock-filters li .is-active:hover,
.client-say:before,
.client-say:after,
.entry-content .entry-post-title h2 a:hover
";
$woo_accent_change_color = "
.woocommerce-breadcrumb a:hover
";

$accent_change_background = "
.gridblock-displayed .gridblock-selected-icon,
.skillbar-title,
.skillbar-bar,
.cart-contents,
#wp-calendar caption,
#wp-calendar tbody td a
";
$woo_accent_change_background = "
.woocommerce span.onsale, .woocommerce-page span.onsale
";

$accent_change_border = "
ul#thumb-list li.current-thumb,
ul#thumb-list li.current-thumb:hover,
.home-step:hover .step-element img,
.home-step-wrap li,
.gridblock-element:hover,
.gridblock-grid-element:hover,
.gridblock-displayed:hover,
.ui-tabs .ui-tabs-nav .ui-state-active a,
.ui-tabs .ui-tabs-nav .ui-state-active a:hover,
.entry-content blockquote,
#gridblock-filters li .is-active,
#gridblock-filters li a:focus,
#gridblock-filters a:focus,
#gridblock-filters li .is-active,
#gridblock-filters li .is-active:hover,
.main-menu-wrap .homemenu .sf-menu .mega-item .children-depth-0,
.main-menu-wrap .homemenu ul ul
";

	$dynamic_css .= mtheme_change_class($accent_change_color,"color",$accent_color,'');
	$dynamic_css .= mtheme_change_class($woo_accent_change_color,"color",$accent_color,'important');

	$dynamic_css .= mtheme_change_class($accent_change_background,"background-color",$accent_color,'');
	$dynamic_css .= mtheme_change_class($woo_accent_change_background,"background-color",$accent_color,'important');

	$dynamic_css .= mtheme_change_class($accent_change_border,"border-color",$accent_color,'');

	$dynamic_css .= ".wp-accordion h3.ui-state-active { border-bottom-color:".$accent_color.";}";
	$dynamic_css .= ".calltype-line-right .callout { border-right-color:".$accent_color.";}";
	$dynamic_css .= ".calltype-line-left .callout { border-left-color:".$accent_color.";}";
	$dynamic_css .= ".calltype-line-top .callout { border-top-color:".$accent_color.";}";
	$dynamic_css .= ".calltype-line-bottom .callout { border-bottom-color:".$accent_color.";}";
}



$top_margin_space=of_get_option('top_margin_space');
if ($top_margin_space!=0 && isSet($top_margin_space)) {
	$dynamic_css .= '.container-outer { margin-top: '.$top_margin_space.'px; }';
}

// Menu colors

$menu_title_color=of_get_option('menu_title_color');
if ($menu_title_color) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul li a, .mobile-menu-selected',"color",$menu_title_color,'');
}

$menu_titlelinkhover_color=of_get_option('menu_titlelinkhover_color');
if ($menu_titlelinkhover_color) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul li a:hover',"color",$menu_titlelinkhover_color,'');
}

$menusubcat_bgcolor=of_get_option('menusubcat_bgcolor');
if ($menusubcat_bgcolor) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul ul li',"background-color",$menusubcat_bgcolor,'');
}
$menusubcat_bgcolor=of_get_option('menusubcat_bghovercolor');
if ($menusubcat_bgcolor) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul ul li:hover',"background-color",$menusubcat_bgcolor,'');
}
$menusubcat_linkcolor=of_get_option('menusubcat_linkcolor');
if ($menusubcat_linkcolor) {
	$dynamic_css .= mtheme_change_class('body .main-menu-wrap .homemenu ul ul li a,body .main-menu-wrap .homemenu ul ul li a,body.background-ui-dark.dual-logo-support .homemenu ul ul li a,.fullscreen-slide-dark.dual-logo-support .homemenu ul ul li a ',"color",$menusubcat_linkcolor,'');
}
$menusubcat_linkhovercolor=of_get_option('menusubcat_linkhovercolor');
if ($menusubcat_linkhovercolor) {
	$dynamic_css .= mtheme_change_class('body .main-menu-wrap .homemenu ul ul li:hover>a',"color",$menusubcat_linkhovercolor,'');
}
$menusubcat_link2color=of_get_option('menusubcat_link2color');
if ($menusubcat_link2color) {
	$dynamic_css .= mtheme_change_class('body .main-menu-wrap .homemenu .top-menu ul.sub-menu li ul.sub-menu li > a',"color",$menusubcat_link2color,'');
}
$menusubcat_link2hovercolor=of_get_option('menusubcat_link2hovercolor');
if ($menusubcat_link2hovercolor) {
	$dynamic_css .= mtheme_change_class('body .main-menu-wrap .homemenu .top-menu ul.sub-menu li ul.sub-menu li:hover > a',"color",$menusubcat_link2hovercolor,'');
}
$menusubcat_bg2hovercolor=of_get_option('menusubcat_bg2hovercolor');
if ($menusubcat_bg2hovercolor) {
	$dynamic_css .= mtheme_change_class('.homemenu ul ul li:hover, .homemenu .top-menu ul.sub-menu li ul.sub-menu li:hover',"background-color",$menusubcat_bg2hovercolor,'');
}
$menusubcat_bg2color=of_get_option('menusubcat_bg2color');
if ($menusubcat_bg2color) {
	$dynamic_css .= mtheme_change_class('.homemenu ul ul li:hover, .homemenu .top-menu ul.sub-menu li ul.sub-menu li',"background-color",$menusubcat_bg2color,'');
}

$share_icon_color=of_get_option('share_icon_color');
if ($share_icon_color) {
	$dynamic_css .= mtheme_change_class('.social-toggle i,.social-header-wrap ul li.social-icon i',"color",$share_icon_color,'important');
}
$share_icon_hovercolor=of_get_option('share_icon_hovercolor');
if ($share_icon_hovercolor) {
	$dynamic_css .= mtheme_change_class('.social-toggle i:hover,.social-header-wrap ul li.social-icon i:hover',"color",$share_icon_hovercolor,'important');
}

$menu_stickymenu_bgcolor=of_get_option('menu_stickymenu_bgcolor');
$menu_stickymenu_bgcolor_rgb=mtheme_hex2RGB($menu_stickymenu_bgcolor,true);
if ($menu_stickymenu_bgcolor) {
	$dynamic_css .= '.sticky-menu-activate { background: rgba('.$menu_stickymenu_bgcolor_rgb.',0.8); }';
}

// General background
$general_bgcolor = of_get_option('general_background_color');
if ($general_bgcolor) {
	$dynamic_css .= mtheme_change_class( 'body',"background-color", $general_bgcolor,'' );
}
$page_background=of_get_option('page_background');
$page_background_rgb=mtheme_hex2RGB($page_background,true);
if ($page_background) {
	$dynamic_css .= '.fullpage-contents-wrap, .sc_slideshowtitle, .entry-page-wrapper, .mtheme_portfolio .container, .portfolio-content-column, .contents-wrap, .woocommerce #container,.entry-title,.fullwidth-theme .container-fullwidth { background: rgba('. $page_background_rgb .',0.9); }';
}
$page_opacity_customize=of_get_option('page_opacity_customize');
if ($page_opacity_customize) {
	$page_background_opacity=of_get_option('page_background_opacity')/100;
$dynamic_css .= '.fullpage-contents-wrap, .sc_slideshowtitle, .entry-page-wrapper, .mtheme_portfolio .container, .portfolio-content-column, .contents-wrap, .woocommerce #container,.entry-title,.fullwidth-theme .container-fullwidth { background: rgba('. $page_background_rgb .','.$page_background_opacity.'); }';
}

$page_title_color=of_get_option('page_title_color');
if ($page_title_color) {
	$dynamic_css .= '.entry-title h1, .entry-title h2 { color: '. $page_title_color .'; }';
}
$page_titlesection_color=of_get_option('page_titlesection_color');
if ($page_titlesection_color) {
	$dynamic_css .= '.entry-title h1, .entry-title h2 { border-bottom-color: '. $page_titlesection_color .'; }';
}

$page_contentscolor=of_get_option('page_contentscolor');
if ($page_contentscolor) {
	$dynamic_css .= mtheme_change_class( '.woocommerce .entry-summary div[itemprop="description"],.entry-content,.entry-content .pullquote-left,.entry-content .pullquote-right,.entry-content .pullquote-center', "color",$page_contentscolor,'' );
}
$page_contentsheading=of_get_option('page_contentsheading');
if ($page_contentsheading) {
$content_headings = '
.woocommerce div.product .product_title,
.woocommerce #content div.product .product_title,
.woocommerce-page div.product .product_title,
.woocommerce-page #content div.product .product_title,
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6
';
	$dynamic_css .= mtheme_change_class( $content_headings, "color",$page_contentsheading,'' );
}

//Footer Colors
$footer_copyrighttext=of_get_option('footer_copyrighttext');
if ($footer_copyrighttext) {
	$dynamic_css .= mtheme_change_class( '#copyright', "color",$footer_copyrighttext,'' );
}

//Sidebar Colors
$sidebar_bgcolor=of_get_option('sidebar_bgcolor');
$sidebar_bgcolor_rgb=mtheme_hex2RGB($sidebar_bgcolor,true);
if ($sidebar_bgcolor) {
	$dynamic_css .= '.sidebar-wrapper { background:rgba('. $sidebar_bgcolor_rgb .',0.96); }';
}
$sidebar_opacity_customize=of_get_option('sidebar_opacity_customize');
if ($sidebar_opacity_customize) {
	$sidebar_background_opacity=of_get_option('sidebar_background_opacity')/100;
	$dynamic_css .= '.sidebar-wrapper { background: rgba('. $sidebar_bgcolor_rgb .','.$sidebar_background_opacity.'); }';
}
$sidebar_headingcolor=of_get_option('sidebar_headingcolor');
if ($sidebar_headingcolor) {
	$dynamic_css .= mtheme_change_class( '.sidebar h3', "color",$sidebar_headingcolor,'' );
}
$sidebar_linkcolor=of_get_option('sidebar_linkcolor');
if ($sidebar_linkcolor) {
	$dynamic_css .= mtheme_change_class( '#recentposts_list .recentpost_info .recentpost_title, #popularposts_list .popularpost_info .popularpost_title,.sidebar a', "color",$sidebar_linkcolor,'' );
}
$sidebar_linkbordercolor=of_get_option('sidebar_linkbordercolor');
if ($sidebar_linkbordercolor) {
	$dynamic_css .= mtheme_change_class( '.sidebar a', "border-color",$sidebar_linkbordercolor,'' );
}
$sidebar_textcolor=of_get_option('sidebar_textcolor');
if ($sidebar_textcolor) {
	$dynamic_css .= mtheme_change_class( '.contact_address_block .about_info, #footer .contact_address_block .about_info, #recentposts_list p, #popularposts_list p,.sidebar ul#recentcomments li,.sidebar', "color",$sidebar_textcolor,'' );
}

if ( of_get_option('custom_font_css')<>"" ) {
	$dynamic_css .= of_get_option('custom_font_css');
}

$portfolio_margin=of_get_option('portfolio_margin');
if (MTHEME_DEMO_STATUS) {
	if ( isSet( $_SESSION['demo_portfolio_margin'] ) || isSet( $_GET['demo_portfolio_margin'] ) ) {
		if ( isSet( $_GET['demo_portfolio_margin'] ) ) $_SESSION['demo_portfolio_margin']=$_GET['demo_portfolio_margin'];
		if ( isSet($_SESSION['demo_portfolio_margin'] )) $demo_portfolio_margin = $_SESSION['demo_portfolio_margin'];
		if (isSet($demo_portfolio_margin)) {
			$portfolio_margin=$demo_portfolio_margin;
		}
	}
}
if ($portfolio_margin) {
$dynamic_css .= '
.gridblock-element-inner,.gridblock-thumbnail-image-wrap {
	margin:0 4px;
}';
}

$photowall_title_color=of_get_option('photowall_title_color');
if ($photowall_title_color) {
$dynamic_css .= mtheme_change_class( '.photowall-title', "color",$photowall_title_color,'' );
}

$photowall_description_color=of_get_option('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= mtheme_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

$photowall_hover_titlecolor=of_get_option('photowall_hover_titlecolor');
if ($photowall_hover_titlecolor) {
$dynamic_css .= mtheme_change_class( '.photowall-item:hover .photowall-title', "color",$photowall_hover_titlecolor,'' );
}
$photowall_hover_descriptioncolor=of_get_option('photowall_hover_descriptioncolor');
if ($photowall_hover_descriptioncolor) {
$dynamic_css .= mtheme_change_class( '.photowall-item:hover .photowall-desc', "color",$photowall_hover_descriptioncolor,'' );
}

$photowall_description_color=of_get_option('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= mtheme_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

$photowall_customize = of_get_option('photowall_customize');
if ($photowall_customize) {

$dynamic_css .= '
.photowall-content-wrap {
	transition: background 1s;
	-moz-transition: background 1s;
	-webkit-transition: background 1s;
	-o-transition: background 1s;
}

.photowall-item:hover .photowall-content-wrap {
	transition: background 0.5s;
	-moz-transition: background 0.5s;
	-webkit-transition: background 0.5s;
	-o-transition: background 0.5s;
}
.photowall-item .photowall-desc,
.photowall-item .photowall-title {
	transition: color 0.5s;
	-moz-transition: color 0.5s;
	-webkit-transition: color 0.5s;
	-o-transition: color 0.5s;
}
.photowall-item:hover .photowall-desc,
.photowall-item:hover .photowall-title {
	transition: color 0.5s;
	-moz-transition: color 0.5s;
	-webkit-transition: color 0.5s;
	-o-transition: color 0.5s;
}';

	$photowall_default_overlaycolor=of_get_option('photowall_default_overlaycolor');
	$photowall_default_overlaycolor_rgb=mtheme_hex2RGB($photowall_default_overlaycolor,true);
	$photowall_default_opacity_percent = of_get_option('photowall_default_opacity')/100;

	if ($photowall_default_overlaycolor) {
		$dynamic_css .= '.photowall-content-wrap { opacity:1; background: rgba('.$photowall_default_overlaycolor_rgb.','.$photowall_default_opacity_percent.'); }';
	} else {
		$dynamic_css .= '.photowall-content-wrap { opacity:1; background: rgba(0,0,0,'.$photowall_default_opacity_percent.'); }';
	}

	$photowall_hover_overlaycolor=of_get_option('photowall_hover_overlaycolor');
	$photowall_hover_overlaycolor_rgb=mtheme_hex2RGB($photowall_hover_overlaycolor,true);
	$photowall_hover_opacity_percent = of_get_option('photowall_hover_opacity')/100;

	if ($photowall_hover_overlaycolor) {
		$dynamic_css .= '.photowall-item:hover .photowall-content-wrap { opacity:1; background: rgba('.$photowall_hover_overlaycolor_rgb.','.$photowall_hover_opacity_percent.'); }';
	} else {
		$dynamic_css .= '.photowall-item:hover .photowall-content-wrap { opacity:1; background: rgba(0,0,0,'.$photowall_hover_opacity_percent.'); }';
	}
}

$blog_allowedtags=of_get_option('blog_allowedtags');
if ($blog_allowedtags) {
$dynamic_css .= '.form-allowed-tags { display:none; }';
}
$dynamic_css .= stripslashes_deep( of_get_option('custom_css') );
?>