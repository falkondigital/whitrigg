<?php
/*
 Single Portfolio Page
*/
?>
<?php get_header(); ?>
<?php
/**
*  Portfolio Loop
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php

		global $mtheme_portfolio_current_post;
		$mtheme_portfolio_current_post=$post;
		$width=MTHEME_FULLPAGE_WIDTH;
		$portfolio_page_header="";
		$portfolio_client="";
		$portfolio_projectlink="";
		$portfolio_client_link="";
		
		$custom = get_post_custom($post->ID);
		$mtheme_pagestyle="fullwidth";
		if (isset($custom[MTHEME . '_pagestyle'][0])) $mtheme_pagestyle=$custom[MTHEME . '_pagestyle'][0];
		if (isset($custom[MTHEME . '_portfoliotype'][0])) $portfolio_page_header=$custom[MTHEME . '_portfoliotype'][0];
		if (isset($custom[MTHEME . '_video_embed'][0])) $portfolio_videoembed=$custom[MTHEME . '_video_embed'][0];
		if (isset($custom[MTHEME . '_customlink'][0])) $custom_link=$custom[MTHEME . '_customlink'][0];
		if (isset($custom[MTHEME . '_clientname'][0])) $portfolio_client=$custom[MTHEME . '_clientname'][0];
		if (isset($custom[MTHEME . '_clientname_link'][0])) $portfolio_client_link=$custom[MTHEME . '_clientname_link'][0];
		if (isset($custom[MTHEME . '_projectlink'][0])) $portfolio_projectlink=$custom[MTHEME . '_projectlink'][0];
		if (isset($custom[MTHEME . '_skills_required'][0])) $portfolio_skills_required=$custom[MTHEME . '_skills_required'][0];

		if ($mtheme_pagestyle=="fullwidth" || $mtheme_pagestyle=="edgetoedge") { $floatside=""; }
		if ($mtheme_pagestyle=="rightsidebar") { $floatside="float-left"; }
		if ($mtheme_pagestyle=="leftsidebar") { $floatside="float-right"; }

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<?php

		$div_open=false;
		if ($mtheme_pagestyle=="rightsidebar" || $mtheme_pagestyle=="leftsidebar" ) {
		?>
			<div class="<?php echo $floatside; ?> <?php if ($mtheme_pagestyle != "nosidebar") { echo 'two-column'; } ?>">
		<?php
			$div_open=true;
		}
		?>
		<?php
		get_template_part('page','title');
		?>
		<div class="portfolio-content-column">
		<div class="entry-content clearfix">
		<?php
	if ( ! post_password_required() ) {

		echo '<div class="portfolio-fullwidth-column-image">';
		
		switch ($portfolio_page_header) {
		
			case "Slideshow" :
				$flexi_slideshow = do_shortcode('[flexislideshow slideshowtitle=true lightbox=true lboxtitle=true imagesize="gridblock-full"]');
				echo $flexi_slideshow;
				
			break;
			case "Vertical" :
				$mtheme_thepostID=$post->ID;
				//global $mtheme_thepostID;
				$vertical_images = do_shortcode('[vertical_images pageid="'.get_the_id().'" imagesize="gridblock-full"]');
				echo $vertical_images;
			break;
			case "Image" :
				// Show Image			
				echo mtheme_display_post_image (
					$post->ID,
					$have_image_url=false,
					$link=false,
					$type="fullwidth",
					$post->post_title,
					$class="portfolio-single-image" 
				);

			break;
			case "Video" :
				echo '<div class="fitVids">';
					echo $portfolio_videoembed;
				echo '</div>';
			break;
			
		}
	}				
	?>
</div>
<div class="portfolio-single-wrap portfolio-fullwidth-column clearfix">
<?php
if (!isSet($mtheme_pagestyle)) { $mtheme_pagestyle="column"; }
echo '<div class="portfolio-single-fullwidth">';
?>	
	<?php
	if ( post_password_required() ) {
		do_action('mtheme_display_password_form');
	} else {
	?>

			<div class="portfolio-details-wrap">
				<?php
				if (isSet($portfolio_skills_required)) {
				?>
				<div class="column2 column_space">
				<div class="project-details project-skills">
						<h4><?php echo of_get_option('portfolio_skill_refer'); ?></h4>
					<?php
					$skills_user = $portfolio_skills_required;
					$skills = explode(',', $skills_user);

				    $skill_list = '<ul>';
				    	foreach( $skills as $skill ):
				    $skill_list .= '<li>';
				    $skill_list .= $skill;
				    $skill_list .= '</li>';
				    	endforeach;
				    $skill_list .= '</ul>';
				    echo $skill_list;
				    ?>
				</div>
				</div>
				<?php
				}
				?>
				<?php
				if ($portfolio_client) {
				?>
				<div class="column2 clearfix">
				<div class="project-details project-client-info clearfix">
					<?php if ( $portfolio_client ) { echo '<h4>' . of_get_option('portfolio_client_refer') . '</h4>'; } ?>
					<?php if ( $portfolio_client_link !='' ) { echo '<a href="'.$portfolio_client_link .'">'; } ?>
						<?php echo $portfolio_client; ?>
					<?php if ( $portfolio_client_link !='' ) { echo '</a>'; } ?>
				</div>
				</div>
				<?php
				}
				?>
				<div class="project-details-link clear">
				<?php
				if ( isSet($custom[MTHEME . '_projectlink'][0]) ) {
					$portfolio_projectlink=$custom[MTHEME . '_projectlink'][0];
					echo '<i class="fa fa-external-link"></i><h4><a href="'.$portfolio_projectlink.'">'.of_get_option('portfolio_link_text').'</a></h4>';
				}
				?>
				</div>
			</div>
			<div class="clear">
			<?php the_content(); ?>
			</div>
</div>
	<?php
	}
	?>
	<?php endwhile; // end of the loop. ?>
</div>
<?php
if ( ! post_password_required() ) {
	get_template_part('/includes/share','this');
}
?>
	<?php
	if (is_singular('mtheme_portfolio')) {
		if ( ! post_password_required() ) {
			if (of_get_option('portfolio_recently')) {
		?>
		<div class="recent-portfolio-single">
			<div class="recent-single-carousel-wrap">
				<h3 class="item-title"><?php _e('Recently in Portfolio','mthemelocal'); ?></h3>
			<?php
			$orientation = of_get_option('portfolio_related_format');
			if ($orientation == 'portrait') {
				$column_slots = 3;
			} else {
				$column_slots = 2;
			}
			echo do_shortcode('[workscarousel format="'.$orientation.'" worktype_slug="" boxtitle=true columns='.$column_slots.']');
			?>
			</div>
		</div>
		<?php
			}
		}
	}
	?>
<?php
if ( ! post_password_required() ) {
if (of_get_option('portfolio_comments')) {
	if ( comments_open() ) {
	?>
	<div class="fullpage-contents-wrap">
	<?php
		comments_template();
	?>
	</div>
	<?php
	}
}
}
?>
</div>
</div>
<?php
if ($div_open) {
	echo '</div>';
}
?>
</div>
<?php
	global $mtheme_pagestyle;
	if ($mtheme_pagestyle=="rightsidebar" || $mtheme_pagestyle=="leftsidebar" ) {
		get_sidebar();
	}
?>
<?php get_footer(); ?>