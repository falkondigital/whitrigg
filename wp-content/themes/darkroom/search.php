<?php
/*
*  Search Page
*/
?>
 
<?php get_header(); ?>
<?php
global $mtheme_pagelayout_type;
$mtheme_pagelayout_type="two-column";
?>

	<?php if ( have_posts() ) : ?>

	<div class="float-left two-column">
		<?php
		get_template_part('page','title');
		?>
		<div class="archive-page-wrapper">

		<?php
			get_template_part( 'loop', 'search' );
		?>
		</div>
	</div>
	<?php else : ?>
	<div class="float-left two-column">
		<?php
		get_template_part('page','title');
		?>
		<div class="entry-page-wrapper no-search-results entry-content clearfix">
			<h2><?php _e( 'Nothing Found', 'mthemelocal' ); ?></h2>
			<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with different keywords.', 'mthemelocal' ); ?></p>
			<?php get_search_form(); ?>
		</div>
	</div>

	<?php endif; ?>


<?php get_sidebar(); ?>

<?php get_footer(); ?>