<?php
/*
404 Page
*/
?>
 
<?php get_header(); ?>

<?php
$default_bg= of_get_option('general_background_image');
$default_404_bg= of_get_option('general_404_image');
if (!isSet($default_404_bg)) {
	$default_404_bg = $default_bg;
}
?>
<script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function(){
<?php
	echo '
		jQuery.backstretch("'.$default_404_bg.'", {
			speed: 1000
		});
		';
?>
});
/* ]]> */
</script>
<div class="page-contents-wrap">
	<h2 class="404_error_message"><?php echo of_get_option('404_title') ?></h2>
	<h4><?php _e( 'Try searching...', 'mthemelocal' ); ?></h4>
	<?php get_search_form(); ?>
</div>

<?php get_footer(); ?>