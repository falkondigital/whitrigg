<?php
$postformat = get_post_format();
?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="post-<?php echo $postformat; ?>-wrapper">
			<?php get_template_part( 'includes/postformats/postformat-media' );	?>
			<?php get_template_part( 'includes/postformats/default' );	?>
			<?php
			get_template_part('/includes/share','this');
			?>
			
			<?php comments_template(); ?>
		</div>

	</div>
<?php endwhile; // end of the loop. ?>