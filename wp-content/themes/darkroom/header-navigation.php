<?php
$site_layout_width=of_get_option('general_theme_page');
if (MTHEME_DEMO_STATUS) {
	if ( isSet( $_SESSION['demo_layout'] ) || isSet( $_GET['demo_layout'] ) ) {
		if ( isSet( $_GET['demo_layout'] ) ) $_SESSION['demo_layout']=$_GET['demo_layout'];
		if ( isSet($_SESSION['demo_layout'] )) $demo_layout = $_SESSION['demo_layout'];
		if (isSet($demo_layout)) {
			$site_layout_width=$demo_layout;
		}
	}
}
?>
	<div class="stickymenu-zone header-block-wrap">
	<div class="header-elements-wrap">
		<div class="logo">
			<a href="<?php echo home_url(); ?>/">
				<?php
				$general_theme_style=of_get_option('general_theme_style');
				$main_logo=of_get_option('main_logo');
				$main_logo_dual=of_get_option('main_logo_dual');
				
				$main_header_type = of_get_option('main_header_type');

				if (! MTHEME_DEMO_STATUS) {
					if ( $main_logo<>"" ) {
						echo '<img class="logo-theme-light" src="'.$main_logo.'" alt="logo" />';
					}
					if ($main_header_type == "transparent") {
						if ($main_logo_dual <> "") {
							echo '<img class="logo-theme-dark" src="'.$main_logo_dual.'" alt="logo" />';
						}
					}
					if ( $main_logo == "" && $main_logo_dual == "" ) {
						echo '<img class="logo-theme-light" src="'.MTHEME_PATH.'/images/logo.png" alt="logo" />';
						echo '<img class="logo-theme-dark" src="'.MTHEME_PATH.'/images/logo_dark.png" alt="logo" />';
					}
				} else {
					if ( isset( $_SESSION['demo_theme_style'] ) ) {
						if ( isSet( $_GET['demo_theme_style'] ) ) $_SESSION['demo_theme_style']=$_GET['demo_theme_style'];
						if ( isSet($_SESSION['demo_theme_style'] )) $demo_theme_style = $_SESSION['demo_theme_style'];
						if (isSet($demo_theme_style)) {
							$general_theme_style=$demo_theme_style;
						}
					}
					echo '<img class="logo-theme-light" src="'.MTHEME_PATH.'/images/logo.png" alt="logo" />';
					echo '<img class="logo-theme-dark" src="'.MTHEME_PATH.'/images/logo_dark.png" alt="logo" />';
				}
				?>
			</a>
		</div>
		<div class="main-menu-wrap">

			<div class="menu-toggle menu-toggle-off"><i class="fa fa-times"></i></div>
			<nav>
				<div class="mainmenu-navigation clearfix">
					<?php
					if ( function_exists('wp_nav_menu') ) { 
						// If 3.0 menus exist
						require ( MTHEME_INCLUDES . 'menu/call-menu.php' );

					} else {
					?>
					<ul>
						<li>
							<a href="<?php echo home_url(); ?>/"><?php _e('Home','mthemelocal'); ?></a>
						</li>
					</ul>
					<?php
					}
					?>
				</div>
			</nav>
		</div>
		<?php
		$header_social_status = of_get_option('header_social_status');
		if ($header_social_status) {
		?>
		<div class="social-toggle-wrap">
			<div class="social-toggle-wrap-inner">
				<div class="social-toggle"><i class="fa fa-share-alt"></i></div>
				<div class="header-widgets">
					<?php if ( !function_exists('dynamic_sidebar') 
				
						|| !dynamic_sidebar('Social Header') ) : ?>
				
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
		}
		?>
	</div>
</div>