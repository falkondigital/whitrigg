<?php
/*
* Footer
*/
?>
<?php
function mtheme_display_bg_image() {
	global $mtheme_bg_image_script;
	if ( isSet($mtheme_bg_image_script) ){
		echo $mtheme_bg_image_script;
	}
}
if ( mtheme_is_fullscreen_post() ) {
	$fullscreen_type = mtheme_get_fullscreen_type();
	if ($fullscreen_type=="photowall") {
		add_action( 'wp_footer', 'mtheme_display_bg_image');
	}
	if ($fullscreen_type=="carousel") {
		add_action( 'wp_footer', 'mtheme_display_bg_image');
	}
}
?>
<?php
if ( !mtheme_is_fullscreen_post() && !is_404() && !is_page_template('template-blank.php') ) {
	add_action( 'wp_footer', 'mtheme_display_bg_image');
?>
	</div>
	<?php
	if (MTHEME_BUILDMODE) {
		wp_reset_query();
		if (is_singular()) {
			edit_post_link( __('edit this entry','mthemelocal') ,'<p class="edit-entry">','</p>');
		}
	}
	?>
	<?php //End of Container Wrapper ?>
	<div class="clearfix"></div>
	</div>
	</div>
	<div id="copyright">
	<?php echo stripslashes_deep( of_get_option('footer_copyright') ); ?>
	</div>
<?php
}
?>
<?php
wp_footer();
?>
</body>
</html>