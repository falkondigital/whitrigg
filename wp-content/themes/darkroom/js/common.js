jQuery(document).ready(function($){
	"use strict";

	if ( $('body').hasClass('rightclick-block') ) {
		$(window).on("contextmenu", function(b) {
		    if (3 === b.which) {
		    	showCopyright();
		    	return false;
		    }
		});
	}
	function showCopyright() {
	    $("#dimmer").fadeIn();
	    $("#dimmer").click(function(b) {
	        $(this).fadeOut()
	    });
	}

	var deviceAgent = navigator.userAgent.toLowerCase();
	var isIOS = deviceAgent.match(/(iphone|ipod|ipad)/);
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
	var curr_menu_item;
	var percent;

	if (isIOS || isAndroid) {
		$('.fullpage-block').css('background-attachment','scroll');
	}
	$('.side-dashboard-toggle').live("click", function(){
		//dashboard toggle
		$('body').toggleClass('body-dashboard-push-right');
		$('.side-dashboard-wrap').toggleClass('dashboard-push-onscreen');
	});
	$(".mobile-menu-icon").click(function(){
		//mobile menu
		$('body').toggleClass('body-dashboard-push-left');
		$(".responsive-mobile-menu").toggleClass('menu-push-onscreen');
	});
	$('.container-wrapper').click(function(){
		//Reset dashboard
		$('body').removeClass('body-dashboard-push-right');
		$('.side-dashboard-wrap').removeClass('dashboard-push-onscreen');
		//reset mobile menu
		$('body').removeClass('body-dashboard-push-left');
		$(".responsive-mobile-menu").removeClass('menu-push-onscreen');
	});

    $(window).resize(function() {
    	//Reset dashboard
		$('body').removeClass('body-dashboard-push-right');
		$('.side-dashboard-wrap').removeClass('dashboard-push-onscreen');
		//reset mobile menu
		$('body').removeClass('body-dashboard-push-left');
		$(".responsive-mobile-menu").removeClass('menu-push-onscreen');
    });

	if (isIOS || isAndroid) {
		$('.fullpage-block').css('background-attachment','scroll');
	}

	$(".ntips").tooltip({
		position: { 
			my: "center bottom+40",
			at: "center bottom"
		},
		show: {
		effect: "slideDown",
		delay: 50
		}
	});

	$(".fitVids").fitVids();
	
	$('.homemenu ul.sf-menu').superfish({
		animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation
		speed:         	'fast',
		disableHI:     	true,
		delay: 			1000,
		autoArrows:  	true,
		dropShadows: 	true
		});

	var fullscreen_toggle_elements = $(".container-outer,#copyright,.edit-entry,.mainmenu-navigation,.social-toggle-wrap");
	var slideshow_caption = $('#static_slidecaption,#slidecaption');
	//Sidebar toggle function
	$(".menu-toggle-off").live('click',function () {
		$('.header-block-wrap').addClass('header-background-off');
		$('.logo').hide();
		$('body').removeClass('fullscreen-mode-off').addClass('fullscreen-mode-on');
		$('.menu-toggle').removeClass('menu-toggle-off').addClass('menu-toggle-on');
		
		$('.background-slideshow-controls').fadeIn();

		fullscreen_toggle_elements.fadeOut();
		slideshow_caption.css('left','-9999999px');

	});
	
	//Sidebar toggle function
	$(".menu-toggle-on").live('click',function () {
		$('.header-block-wrap').removeClass('header-background-off');
		$('.logo').show();
		$('body').removeClass('fullscreen-mode-on').addClass('fullscreen-mode-off');
		$('.menu-toggle').removeClass('menu-toggle-on').addClass('menu-toggle-off');
		
		$('.background-slideshow-controls').fadeOut();

		fullscreen_toggle_elements.fadeIn();
		slideshow_caption.css('left','0');

		var $filterContainer = $('#gridblock-container');
		if ($.fn.isotope) {
			$filterContainer.isotope({
			animationEngine : 'best-available',
			layoutMode : 'fitRows',
			  masonry: {
			    gutterWidth: 0
			  }
			});
		}
	});

	$(".social-toggle-wrap-inner").hover(
	function () {
	  $(this).find(".header-widgets").stop().fadeIn();
	},
	function () {
	  $(this).find(".header-widgets").stop().delay('500').fadeOut();
	});

	
	//Portfolio Hover effects
	$(".gototop,.hrule.top a").click(function(){
		$('html, body').animate({
			scrollTop:0
		},{
	        duration: 1200,
	        easing: "easeInOutExpo"
	    });
		return false;
	});
	
	// Responsive dropdown list triggered on Mobile platforms
    $('#top-select-menu').bind('change', function () { // bind change event to select
        var url = $(this).val(); // get selected value
        if (url != '') { // require a URL
            window.location = url; // redirect
        }
        return false;
    });
	
	//Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
	$(".toggle-shortcode").click(function(){
		$(this).toggleClass("active").next().slideToggle("fast");
		return false;
	});

	$(".service-item").hover(
	function () {
		$(this).children('.icon-large').animate({
			top:-10
		},300);
	},
	function () {
		$(this).children('.icon-large').animate({
			top:0
		},300);
	});
	
	$("#main-gridblock-carousel .preload").hover(
	function () {
	  $(this).stop().fadeTo("fast", 0.6);
	},
	function () {
	  $(this).stop().fadeTo("fast", 1);
	});
	
	$(".gridblock-image-holder").hover(
	function () {
	  $(this).stop().fadeTo("fast", 0.6);
	},
	function () {
	  $(this).stop().fadeTo("fast", 1);
	});
	
	$(".thumbnail-image").hover(
	function () {
	  $(this).stop().fadeTo("fast", 0.6);
	},
	function () {
	  $(this).stop().fadeTo("fast", 1);
	});
	
	$(".pictureframe").hover(
	function () {
	  $(this).stop().fadeTo("fast", 0.6);
	},
	function () {
	  $(this).stop().fadeTo("fast", 1);
	});
	
	$(".filter-image-holder").hover(
	function () {
	  $(this).stop().fadeTo("fast", 0.6);
	},
	function () {
	  $(this).stop().fadeTo("fast", 1);
	});

	//Tool Tip
	$('.qtips').tipsy({gravity: 'e'});
	$('.etips').tipsy({gravity: 'e'});
	$('.stips').tipsy({gravity: 's'});

	$("#popularposts_list li:even,#recentposts_list li:even").addClass('even');
	$("#popularposts_list li:odd,#recentposts_list li:odd").addClass('odd');

	$(".cart_table_item:even").addClass('even');
	$(".cart_table_item:odd").addClass('odd');
	
	$(".close_notice").click(function(){
	  	$(this).parent('.noticebox').fadeOut();
	});

	if ($.fn.waypoint) {

		//Skill Bar
		$('.skillbar').waypoint(function() {
			$('.skillbar').each(function(){
				percent = $(this).attr('data-percent');
				percent = percent + '%';
				$(this).find('.skillbar-bar').animate({
					width: percent
				},1500);
			});
		}, { offset: 'bottom-in-view' });

		$('.is-animated').waypoint(function() {
			$(this).removeClass('is-animated').addClass('element-animate');
		}, { offset: 'bottom-in-view' });
	}
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#goto-top').fadeIn();
			} else {
				$('#goto-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#goto-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

	// WooCommerce Codes
	// Thumnail hover for secondary image
	$( 'ul.products li.mtheme-hover-thumbnail' ).hover( function() {
		console.log('hovered');
		var woo_secondary_thumbnail = $(this).find( '.mtheme-secondary-thumbnail-image' ).attr('src');
		if( woo_secondary_thumbnail !== undefined ) {
			$( this ).find( '.wp-post-image' ).removeClass( 'woo-thumbnail-fadeInDown' ).addClass( 'woo-thumbnail-fadeOutUp' );
			$( this ).find( '.mtheme-secondary-thumbnail-image' ).removeClass( 'woo-thumbnail-fadeOutUp' ).addClass( 'woo-thumbnail-fadeInDown' );
		}
	}, function() {
		var woo_secondary_thumbnail = $(this).find( '.mtheme-secondary-thumbnail-image' ).attr('src');
		if( woo_secondary_thumbnail !== undefined ) {
			$( this ).find( '.wp-post-image' ).removeClass( 'woo-thumbnail-fadeOutUp' ).addClass( 'woo-thumbnail-fadeInDown' );
			$( this ).find( '.mtheme-secondary-thumbnail-image' ).removeClass( 'woo-thumbnail-fadeInDown' ).addClass( 'woo-thumbnail-fadeOutUp' );
		}
	});


	var woocommerce_ordering = $(".woocommerce-page .woocommerce-ordering select");
	if ( (woocommerce_ordering).length ) {
		var woocommerce_ordering_curr = $(".woocommerce-ordering select option:selected").text();
		var woocommerce_ordering_to_ul = woocommerce_ordering
			.clone()
			.wrap("<div></div>")
			.parent().html()
			.replace(/select/g,"ul")
			.replace(/option/g,"li")
			.replace(/value/g,"data-value");

		$( '.woocommerce-ordering' )
		.prepend( '<div class="mtheme-woo-order-selection-wrap"><div class="mtheme-woo-order-selected-wrap"><span class="mtheme-woo-order-selected">'+woocommerce_ordering_curr+'</span><i class="fa fa-reorder"></i></div><div class="mtheme-woo-order-list">' + woocommerce_ordering_to_ul + '</div></div>' );
	}

	$(function(){
		//$('.woocommerce-page .woocommerce-ordering select').hide();
	    $('.mtheme-woo-order-selected-wrap').click(function(){
	        $('.mtheme-woo-order-list ul').fadeToggle('fast');        
	    });
	    $('.mtheme-woo-order-list ul li').click(function(e){

	    	//Set value
	    	 var selected_option = $(this).data('value');
	         $(".woocommerce-page .woocommerce-ordering select").val(selected_option).trigger('change');
	        
	         $('.mtheme-woo-order-selected').text($(this).text());
	         $('.mtheme-woo-order-list').slideUp('fast'); 
	        $(this).addClass('current');
	        e.preventDefault();
	    })
	});

});

//WooCommerce Codes
(function($){
$(window).load(function(){
  // The slider being synced must be initialized first
	if ($.fn.flexslider) {
		$('#mtheme-flex-carousel').flexslider({
			animation: "slide",
			controlNav: false,
			directionNav: false,
			animationLoop: false,
			slideshow: false,
			itemWidth: 100,
			asNavFor: '#mtheme-flex-slider'
		});

		$('#mtheme-flex-slider').flexslider({
			animation: "slide",
			controlNav: false,
			animationLoop: true,
			slideshow: true,
			sync: "#mtheme-flex-carousel"
		});
	}
})
})(jQuery);