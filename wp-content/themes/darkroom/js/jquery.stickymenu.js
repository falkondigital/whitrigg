/*
 * jQuery Sticky Menu by Imaginem
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
*/
jQuery(document).ready(function($){

	"use strict";
	
	var deviceAgent = navigator.userAgent.toLowerCase();
	var isIOS = deviceAgent.match(/(iphone|ipod|ipad)/);
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1;

	var max_menu_width = 650;
	var min_window_height = 600;
	var flip_menu_offset_top = 50;
	var sticky_navigation_offset_top = 600;
	
	var menubar_h = $('.header-elements-wrap').height();

	// Pass if not on iOS or android
	if (isIOS==null || isAndroid==null) {
		var sticky_navigation = function(){
			var scroll_top = $(window).scrollTop();

			if (scroll_top > sticky_navigation_offset_top) {
				$('.stickymenu-zone').addClass('sticky-menu-activate');
			} else {
				sticky_navigation_reset();
			} 
		};

		//Reset and displays the main menu
		var sticky_navigation_reset = function(){
			$('.stickymenu-zone').removeClass('sticky-menu-activate'); 			
		};

		// Initialize
		var sticky_navigation_init = function(){
			// run our function on load
			var windowHeight = $(window).height();
			var menuLength = $('.header-elements-wrap').width();
			if (menuLength > max_menu_width ) {
				sticky_navigation();
			}
		};

		//Scroll Detection
		$(window).scroll(function() {
			var windowHeight = $(window).height();
			sticky_navigation_init();
			var scroll_top = $(window).scrollTop();
			// Remove Sticky menu if at the top
			if (scroll_top < 10 ) {
				$('.stickymenu-zone').removeClass('sticky-menu-activate');
			}
			//On 
		});

		//On Resize reset
		$(window).resize(function() {
			var windowHeight = $(window).height();
			var menuLength = $('.header-elements-wrap').width();
			if (menuLength < max_menu_width ) {
				sticky_navigation_reset();
			}
		});
	}
});