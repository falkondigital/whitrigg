<?php
/*
*  Single Page
*/
?>
<?php get_header(); ?>
<?php
$floatside="";
$mtheme_pagestyle= get_post_meta($post->ID, MTHEME . '_pagestyle', true);
if (!isSet($mtheme_pagestyle) || $mtheme_pagestyle=="") {
	$mtheme_pagestyle="rightsidebar";
}
if ($mtheme_pagestyle != "nosidebar" && $mtheme_pagestyle != "edgetoedge") {
	$floatside="float-left";
	if ($mtheme_pagestyle=="rightsidebar") { $floatside="float-left two-column"; }
	if ($mtheme_pagestyle=="leftsidebar") { $floatside="float-right two-column"; }
}
?>
<div class="single-header <?php echo $floatside; ?>">
<?php
get_template_part('page','title');
?>
<div class="contents-wrap">
<?php
get_template_part( 'loop', 'single' );
?>
</div>
</div>
<?php
if ($mtheme_pagestyle != "nosidebar") {
	global $mtheme_pagestyle;
	if ($mtheme_pagestyle=="rightsidebar" || $mtheme_pagestyle=="leftsidebar" ) {
		get_sidebar();
	}
}
get_footer();
?>