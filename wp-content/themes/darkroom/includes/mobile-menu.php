<div class="responsive-menu-wrap">
	<div class="mobile-menu-toggle">
		<span class="mobile-menu-icon"><i class="fa fa-reorder"></i></span>
				<div class="logo-mobile">
						<?php
						$main_logo=of_get_option('main_logo');
						$responsive_logo=of_get_option('responsive_logo');
						if ( $main_logo<>"" ) {
							if ($responsive_logo<>"") {
								echo '<img class="logoimage" src="' . $responsive_logo .'" alt="logo" />';
							} else {
								echo '<img class="logoimage" src="' . $main_logo .'" alt="logo" />';
							}
						} else {
							echo '<img class="logoimage logo-light" src="'.MTHEME_PATH.'/images/logo_dark.png" alt="logo" />';
						}
						?>
				</div>
	</div>
</div>
<div class="responsive-mobile-menu clearfix">
	<?php
	$wpml_lang_selector_disable= of_get_option('wpml_lang_selector_disable');
	if (!$wpml_lang_selector_disable) {
	?>
	<div class="mobile-wpml-lang-selector-wrap">
		<?php do_action('icl_language_selector'); ?>
	</div>
	<?php
	}
	?>
	<?php
	$custom_menu_call = '';
	if (MTHEME_DEMO_STATUS) {
		if ( is_page('one-page') ) {
			$custom_menu_call = 'One Page';
		}
	}
?>
<div class="mobile-social-header">				
<?php if ( !function_exists('dynamic_sidebar') 

|| !dynamic_sidebar('Social Header') ) : ?>

<?php endif; ?>
</div>
<?php
	get_search_form();
	// Responsive menu conversion to drop down list
	if ( function_exists('wp_nav_menu') ) { 
		wp_nav_menu( array(
		 'container' =>false,
		 'theme_location' => 'top_menu',
		 'menu_class' => 'mobile-menu',
		 'echo' => true,
		 'before' => '',
		 'after' => '',
		 'link_before' => '',
		 'link_after' => '',
		 'depth' => 0,
		 'fallback_cb' => 'mtheme_nav_fallback'
		 )
		);
	}
	?>
</div>