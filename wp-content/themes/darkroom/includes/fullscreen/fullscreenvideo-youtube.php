<?php
$featured_page=mtheme_get_active_fullscreen_post();
$custom = get_post_custom($featured_page);
if (isSet($custom[MTHEME . "_youtubevideo"][0])) $youtube=$custom[MTHEME . "_youtubevideo"][0];
	if ( !wp_is_mobile() ) {
?>
<div id="backgroundvideo">
</div>
<script>
jQuery(document).ready(function($) {
	var options = { videoId: '<?php echo $youtube; ?>', wrapperZIndex: -1, start: 0, mute: false, repeat: false, ratio: 16/9 };
	$('#backgroundvideo').tubular(options);
});
</script>
<?php
	} else {
		if ( has_post_thumbnail()) :
			$default_bg = mtheme_featured_image_link( $featured_page );
		 	?>
			<script type="text/javascript">
			/* <![CDATA[ */
			jQuery(document).ready(function(){
			<?php
				echo '
					jQuery.backstretch("'.$default_bg.'", {
						speed: 1000
					});
					';
			?>
			});
			/* ]]> */
			</script>
		 <?php
		 endif;
		echo '<div class="youtube-button-wrap"><a class="youtube-play" href="http://www.youtube.com/watch?v='.$youtube.'" title="Play"></a></div>';		
	}
?>