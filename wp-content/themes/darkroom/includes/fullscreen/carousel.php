<?php
/**
 * Kenburns
 */
$count=0;
if ( post_password_required($featured_page) ) {
get_header();
// Grab default background set from theme options	
$default_bg= of_get_option('general_background_image');
?>
<script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function(){
<?php
	echo '
		jQuery.backstretch("'.$default_bg.'", {
			speed: 1000
		});
		';
?>
});
/* ]]> */
</script>
<div class="container-wrapper">
	<div class="container-boxed mtheme-adjust-max-height">
		<div class="container fullscreen-protected clearfix">
		<?php
		do_action('mtheme_display_password_form');
		?>
		</div>
	</div>
</div>
<?php	
} else {
if (defined('ICL_LANGUAGE_CODE')) { // this is to not break code in case WPML is turned off, etc.
    $_type  = get_post_type($featured_page);
    $featured_page = icl_object_id($featured_page, $_type, true, ICL_LANGUAGE_CODE);
}
// Don't Populate list if no Featured page is set
//The Image IDs
if ( $featured_page <>"" ) { 

$filter_image_ids = mtheme_get_custom_attachments ( $featured_page );
get_header();
mtheme_populate_slide_ui_colors($featured_page);
if ($filter_image_ids) {
	$count=0;
	$captions='';
	$carousel='';
?>
<div class="circular-preloader"></div>
<div class="fullscreen-horizontal-carousel">
            <span class="colorswitch prev-hcarousel"></span>
            <span class="colorswitch next-hcarousel"></span>
  <div class="horizontal-carousel-outer">
        <div class="horizontal-carousel-inner">
            <div class="horizontal-carousel-wrap">
                <ul class="horizontal-carousel">
<?php		
	// Loop through the images
	foreach ( $filter_image_ids as $attachment_id) {
		$attachment = get_post( $attachment_id );
		$imageURI = $attachment->guid;

		$thumb_imagearray = wp_get_attachment_image_src( $attachment->ID , 'gridblock-full', false);
		$thumb_imageURI = $thumb_imagearray[0];
				
		$imageTitle = apply_filters('the_title',$attachment->post_title);
		$imageDesc = apply_filters('the_content',$attachment->post_content);

		$count++;
		$carousel .= '<li data-id="'.$count.'" data-position="0" data-title="'.$imageTitle.'" class="hc-slides slide-'.$count.'">';
		$carousel .= '<div class="hc-image-wrap">';
		$carousel .= '<img title="'.esc_attr($imageTitle).'" data-lightbox="magnific-carousel-gallery" data-mfp-src="'.$imageURI.'" src="'.$thumb_imageURI.'" alt="thumbnail"/>';
		$carousel .= '<div class="responsive-titles">';
		$carousel .= '<div class="title"><h3 class="colorswitch">'.$imageTitle.'</h3></div>';
		$carousel .= '<div class="description colorswitch">'.$imageDesc.'</div>';
		$carousel .= '</div>';
		$carousel .= '</div>';
		$carousel .= '</li>';

		$captions .= '<li class="caption-'.$count.'" data-id="'.$count.'">';
		$captions .= '<div class="title"><h2 class="colorswitch">'.$imageTitle.'</h2></div>';
		$captions .= '<div class="description colorswitch">'.$imageDesc.'</div>';
		$captions .= '</li>';
	}
	echo $carousel;
?>
              </ul>
            </div>
        </div>
        <div class="carousel-captions">
        <ul>
        	<?php
        	echo $captions;
        	?>
        </ul>
        </div>          
  </div>
</div>
<?php
// If Ends here for the Featured Page
require_once (MTHEME_INCLUDES . 'fullscreen/audioplay.php');
}
}
?>
<?php
//End Password Check
}
?>
<?php get_footer(); ?>