<?php
/**
 * Fullscreen Video
 */
get_header();
$featured_page=mtheme_get_active_fullscreen_post();
$custom = get_post_custom($featured_page);
if (isSet($custom[MTHEME . "_youtubevideo"][0])) $youtube=$custom[MTHEME . "_youtubevideo"][0];
if (isSet($custom[MTHEME . "_vimeovideo"][0])) $vimeoID=$custom[MTHEME . "_vimeovideo"][0];
if (isSet($custom[MTHEME . "_html5_mp4"][0])) $html5_mp4=$custom[MTHEME . "_html5_mp4"][0];
if (isSet($custom[MTHEME . "_html5_wemb"][0])) $html5_wemb=$custom[MTHEME . "_html5_wemb"][0];

$video_control_bar=of_get_option('video_control_bar');
$fullscreen_menu_toggle=of_get_option('fullscreen_menu_toggle');
$fullscreen_menu_toggle_nothome=of_get_option('fullscreen_menu_toggle_nothome');

if ( post_password_required($featured_page) ) {
// Grab default background set from theme options	
$default_bg= of_get_option('general_background_image');
?>
<script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function(){
<?php
	echo '
		jQuery.backstretch("'.$default_bg.'", {
			speed: 1000
		});
		';
?>
});
/* ]]> */
</script>
<div class="container-wrapper">
	<div class="container-boxed mtheme-adjust-max-height">
		<div class="container fullscreen-protected clearfix">
		<?php
		do_action('mtheme_display_password_form');
		?>
		</div>
	</div>
</div>
<?php
	} else {
?>
<?php
$vimeo_active=false;
$youtube_active=false;
$html5_active=false;
// Activate Vimeo iframe for fullscreen playback
if ( isSet($vimeoID) && !empty($vimeoID) ) {
	$vimeo_active=true;
	get_template_part('/includes/fullscreen/fullscreenvideo','vimeo');
}
// Play Youtube and Other Video files
if (isSet($youtube) && !empty($youtube) && !$vimeo_active) {
	$youtube_active=true;
	get_template_part('/includes/fullscreen/fullscreenvideo','youtube');
}
if (isSet($html5_mp4) || isSet($html5_mp4)) {
	if (!$vimeo_active && !$youtube_active) {
		$html5_active=true;
		get_template_part('/includes/fullscreen/fullscreenvideo','html5');
	}
}
//End password check wrap
}
?>
<?php get_footer(); ?>