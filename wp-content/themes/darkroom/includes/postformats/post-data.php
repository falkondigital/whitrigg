<div class="postsummarywrap">

<?php
	$postformat = get_post_format();
	if($postformat == "") $postformat="standard";

$post_icon="";
switch ($postformat) {
	case 'video':
		$postformat_icon = "fa fa-play";
		break;
	case 'audio':
		$postformat_icon = "fa fa-music";
		break;
	case 'gallery':
		$postformat_icon = "fa fa-th-large";
		break;
	case 'quote':
		$postformat_icon = "fa fa-quote-left";
		break;
	case 'link':
		$postformat_icon = "fa fa-link";
		break;
	case 'aside':
		$postformat_icon = "fa fa-file-text-o";
		break;
	case 'image':
		$postformat_icon = "fa fa-picture-o";
		break;
	default:
		$postformat_icon ="fa fa-pencil";
		break;
}
?>
	<div class="datecomment clearfix">
		<?php
		if ( !is_search() ) {
		?>
		<i class="<?php echo $postformat_icon; ?>"></i>
		<span class="post-meta-category">
			<?php the_category(' / ') ?>
		</span>
		<?php
		}
		?>
		<span class="post-single-meta">
			<span class="post-meta-time">
			<i class="fa fa-clock-o"></i>
			<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'mthemelocal' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
			<?php echo esc_attr( get_the_time() ); echo " , "; echo get_the_date(); ?>
			</a>
			</span>
		<?php
		if ( !is_search() ) {
		?>
			<span class="post-meta-comment">
			<i class="fa fa-comments-o"></i>
			<?php comments_popup_link('0', '1', '%'); ?>
			</span>
		<?php
		}
		?>
		</span>
	</div>
</div>
<?php
if ( is_single() ) {
?>
	
	<?php the_tags( '<div class="post-single-tags"><i class="fa fa-tag"></i>', ' ', '</div>'); ?>
<?php
}
?>

<?php //edit_post_link( __('edit this entry','mthemelocal') ,'<p class="edit-entry">','</p>'); ?>