<?php
$media = mtheme_featured_image_link( get_the_id() );
$socialshare = array (
		'facebook' => array (
			'fa-facebook' => 'http://www.facebook.com/sharer.php?u='.get_permalink().'&t='.get_the_title()
			),
		'twitter' => array (
			'fa-twitter' => 'http://twitter.com/home?status='.get_the_title().'+'.get_permalink()
			),
		'googleplus' => array (
			'fa-google-plus' => 'https://plus.google.com/share?url='.get_permalink()
			),
		'pinterest' => array (
			'fa-pinterest' => 'http://pinterest.com/pin/create/bookmarklet/?media='.$media.'&url='.get_permalink().'&is_video=false&description='.get_the_title()
			),
		'link' => array (
			'fa-external-link' => get_permalink()
			),
		'email' => array (
			'fa-envelope' => 'mailto:email@address.com?subject=Interesting Link&body=' . get_the_title() . " " . get_permalink()
			)
		);
?>
<ul class="portfolio-share">
<li class="sharethis"><?php _e('Share','mthemelocal'); ?></li>
<?php
foreach($socialshare as $key => $share){
  foreach( $share as $icon => $url){
    echo '<li class="share-this-'.$icon.'"><a target="_blank" href="'.$url.'"><i class="fa '.$icon.'"></i></a></li>';
  }
}
?>
</ul>