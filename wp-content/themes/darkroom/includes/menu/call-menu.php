<?php
$regular_menu_status=of_get_option('regular_menu_status');
if ($regular_menu_status) {
	$regular_menu_class = ' home-regular-menu';
} else {
	$regular_menu_class = '';
}
if (MTHEME_DEMO_STATUS) {
	if ( isSet( $_SESSION['regular_menu_status'] ) || isSet( $_GET['regular_menu_status'] ) ) {
		if ( isSet( $_GET['regular_menu_status'] ) ) $_SESSION['regular_menu_status']=$_GET['regular_menu_status'];
		if ( isSet($_SESSION['regular_menu_status'] )) $regular_menu_status = $_SESSION['regular_menu_status'];
		if (isSet($regular_menu_status)) {
			if ($regular_menu_status==1) {
				$regular_menu_class=' home-regular-menu';
			}
		}
	}
}
?>
<div class="homemenu<?php echo $regular_menu_class; ?>">
<?php

function mtheme_nav_fallback() {
   require (TEMPLATEPATH . "/includes/menu/fallbackmenu.php");
}

if ( function_exists('wp_nav_menu') ) { 
// If 3.0 menus exist
//	wp_nav_menu( array( 'container' => '', 'theme_location' => 'top_menu', 'fallback_cb' => 'mtheme_nav_fallback' ) );
wp_nav_menu( array(
 'container' =>false,
 'theme_location' => 'top_menu',
 'menu_class' => 'sf-menu sf-navbar',
 'echo' => true,
 'before' => '',
 'after' => '',
 'link_before' => '',
 'link_after' => '',
 'depth' => 0,
 'fallback_cb' => 'mtheme_nav_fallback'
 )
);

} else {
	// Else show the regular Menu
	require (TEMPLATEPATH . "/includes/menu/menu.php");
} 

?>
</div>