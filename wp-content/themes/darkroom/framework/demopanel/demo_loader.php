<?php
function mtheme_demo_scripts_styles() {
?>
<link href="<?php echo get_template_directory_uri(); ?>/framework/demopanel/demo.panel.css" rel="stylesheet" type="text/css" />
<?php
}
function mtheme_demo_scripts_scripts() {
?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/framework/demopanel/js/jquery.cookie.js"></script>
<script type="text/javascript">
/* <![CDATA[ */

jQuery(document).ready(function(){
	var panelClose = jQuery('#demopanel .closedemo');
	var panelOpen = jQuery('#demopanel .opendemo');
	var panelWrap = jQuery('#demopanel');
	var siteBody = jQuery('body');
	var shadeElements = jQuery('.container-head, .flexslider-container, .featured-video-wrapper, .static-featured-image, #footer-container');
	
	jQuery('#demopanel .closedemo').click(function() {
		 panelClose.css('display', 'none');
		 panelOpen.css('display', 'block');
		 panelWrap.stop().animate({ left: '-296'}, {duration: 'fast'});
	});
	jQuery('#demopanel .opendemo').click(function() {
		 panelClose.css('display', 'block');
		 panelOpen.css('display', 'none');
		 panelWrap.stop().animate({ left: '0'}, {duration: 'fast'});
	});

	//jQuery('#demopanel .closedemo').trigger('click');
	
	if ( (jQuery.cookie('themeBG') != null))	{
		jQuery('body').attr("style",jQuery.cookie('themeBG'));
	}
	
    jQuery('a.demo_pattern').click( function() {
        var divId = jQuery(this).attr('id');
		divId=divId.replace('-', '.');
		jQuery("body").removeAttr("style").attr("style","background-image:url(<?php echo get_template_directory_uri(); ?>/images/backgrounds/" + divId + ");");
		jQuery.cookie('themeBG',"background-image:url(<?php echo get_template_directory_uri(); ?>/images/backgrounds/" + divId + ")",{ expires: 7, path: '/'});
		
		var demoBackgroundColor = jQuery('#demo_bg_color').val();
		jQuery('body').css('backgroundColor', demoBackgroundColor );
		
		var demoHeaderColor = jQuery('#demo_header_color').val();
		jQuery('.header-page').css('backgroundColor', demoHeaderColor );
		
	  	return false;
    });
	
	jQuery("#patterndelete").click(function(){
		jQuery.cookie('themeBG',null,{expires: 7, path: '/'});
        return false;
	});
	
	jQuery('#demo_bg_color').bind('change', function () {
		var demoBackgroundColor = jQuery(this).val();
		jQuery('body').css('backgroundColor', demoBackgroundColor );
    });	
	
	jQuery('#demo_header_color').bind('change', function () {
		var demoHeaderColor = jQuery(this).val();
		jQuery('.header-page').css('backgroundColor', demoHeaderColor );
    });	
	
	jQuery('#demo_menu_color').bind('change', function () {
		var demoMenuColor = jQuery(this).val();
		jQuery('.homemenu-wrap,.homemenu ul ul li').css('backgroundColor', demoMenuColor );
    });
});
/* ]]> */
</script>
<?php
	}
	add_action('wp_head','mtheme_demo_scripts_styles',12);
	add_action('wp_footer','mtheme_demo_scripts_scripts',20);
?>