<!-- 
**********************************************************************
DEMO Panel code - START
**********************************************************************
-->
<?php
if ( isset( $_GET['demo_layout'] ) ) $_SESSION['demo_layout']=$_GET['demo_layout'];
if ( isset($_SESSION['demo_layout'] )) $demo_layout = $_SESSION['demo_layout'];

if ( isset( $_GET['demo_skin'] ) ) $_SESSION['demo_skin']=$_GET['demo_skin'];
if ( isset($_SESSION['demo_skin'] )) $demo_skin = $_SESSION['demo_skin'];

if ( isset( $_GET['demo_theme_style'] ) ) $_SESSION['demo_theme_style']=$_GET['demo_theme_style'];
if ( isset($_SESSION['demo_theme_style'] )) $demo_theme_style = $_SESSION['demo_theme_style'];

if ( isset( $_GET['demo_header'] ) ) $_SESSION['demo_header']=$_GET['demo_header'];
if ( isset($_SESSION['demo_header'] )) $demo_header = $_SESSION['demo_header'];

if ( isset( $_GET['demo_header_color'] ) ) $_SESSION['demo_header_color']=$_GET['demo_header_color'];
if ( isset($_SESSION['demo_header_color'] )) $demo_header_color = $_SESSION['demo_header_color'];

if ( isset( $_GET['demo_menu_color'] ) ) $_SESSION['demo_menu_color']=$_GET['demo_menu_color'];
if ( isset($_SESSION['demo_menu_color'] )) $demo_menu_color = $_SESSION['demo_menu_color'];

if ( isset( $_GET['demo_font'] ) ) $_SESSION['demo_font']=$_GET['demo_font'];
if ( isset($_SESSION['demo_font'] )) $heading_font = $_SESSION['demo_font'];

if ( isset( $_GET['demo_title_color'] ) ) $_SESSION['demo_title_color']=$_GET['demo_title_color'];
if ( isset($_SESSION['demo_title_color'] )) $demo_title_color = $_SESSION['demo_title_color'];

if ( isset( $_GET['demo_bg_color'] ) ) $_SESSION['demo_bg_color']=$_GET['demo_bg_color'];
if ( isset($_SESSION['demo_bg_color'] )) $demo_bg_color = $_SESSION['demo_bg_color'];

if ( isset( $_GET['demo_color'] ) ) $_SESSION['demo_color']=$_GET['demo_color'];
if ( isset($_SESSION['demo_color'] )) $demo_color = $_SESSION['demo_color'];
?>
<div id="demopanel">
	<div class="demo_toggle closedemo">
	</div>	
	<div class="demo_toggle opendemo">
	</div>
	
	<div class="paneloptions">
	<form action="#" id="demoform" method="get">
<div class="clear"></div>

<div class="demo_background">

	<div class="demo-main-title">
		Demo Panel
	</div>

	<div class="demo-sub-title">
		Pages
	</div>

	<div class="demo-selector">
		<a href="?demo_theme_style=light">
			<div class="demo-selector-icon demo-selector-light">
				Bright
			</div>
			<div class="demo-selector-label">
				Bright
			</div>
		</a>
	</div>
	<div class="demo-selector">
		<a href="?demo_theme_style=dark">
			<div class="demo-selector-icon demo-selector-dark">
				Dark
			</div>
			<div class="demo-selector-label">
				Dark
			</div>
		</a>
	</div>
<div class="clear"></div>
	<div class="demo-sub-title">
		Header Background
		<p>
			Transparent header supports dual logo as well as single logo.
		</p>
	</div>

	<div class="demo-selector demo-3col">
		<a href="?demo_header_type=bright">
			<div class="demo-selector-icon demo-selector-bright-header">
				Bright
			</div>
			<div class="demo-selector-label">
				Bright
			</div>
		</a>
	</div>

	<div class="demo-selector demo-3col">
		<a href="?demo_header_type=transparent">
			<div class="demo-selector-icon demo-selector-transparent-header">
				Transparent
			</div>
			<div class="demo-selector-label">
				Transparent
			</div>
		</a>
	</div>

	<div class="demo-selector demo-3col">
		<a href="?demo_header_type=dark">
			<div class="demo-selector-icon demo-selector-dark-header">
				Dark
			</div>
			<div class="demo-selector-label">
				Dark
			</div>
		</a>
	</div>
	<div class="clear"></div>
	</div>
	</form>
	
	</div>
</div>
<!-- 
**********************************************************************
DEMO Panel code - END
**********************************************************************
-->
