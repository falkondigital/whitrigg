<?php
/*
*  Page
*/
?>
 
<?php get_header(); ?>
<?php
if ( post_password_required() ) {
	
		do_action('mtheme_display_password_form');
	
} else {
	$mtheme_pagestyle= get_post_meta($post->ID, MTHEME . '_pagestyle', true);
	$floatside="float-left";
	if ($mtheme_pagestyle=="nosidebar") { $floatside=""; }
	if ($mtheme_pagestyle=="rightsidebar") { $floatside="float-left"; }
	if ($mtheme_pagestyle=="leftsidebar") { $floatside="float-right"; }
	?>
	<div class="page-contents-wrap <?php echo $floatside; ?> <?php if ($mtheme_pagestyle != "nosidebar") { echo 'two-column'; } ?>">
	<?php
	get_template_part('page','title');
	?>
	<?php
	get_template_part( 'loop', 'page' );
	?>
	</div>
	<?php
	global $mtheme_pagestyle;
	if ($mtheme_pagestyle=="rightsidebar" || $mtheme_pagestyle=="leftsidebar" ) {
		get_sidebar();
	}
}
?>
<?php get_footer(); ?>