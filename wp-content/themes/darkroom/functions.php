<?php
/**
 * @Functions
 * 
 */
?>
<?php
/*-------------------------------------------------------------------------*/
/* Theme name settings which is shared to some functions */
/*-------------------------------------------------------------------------*/
// Theme Title
$mtheme_ThemeTitle= "Darkroom";
// Theme Name
$mtheme_themename = "Darkroom";
$mtheme_themefolder = "darkroom";
// Notifier Info
$mtheme_notifier_name = "Darkroom";
$mtheme_notifier_url = "";
// Theme name in short
$mtheme_shortname = "mtheme_p2";
if (!defined('MTHEME')) {
	define('MTHEME', $mtheme_shortname);
}
if (!defined('MTHEME_NAME')) {
	define('MTHEME_NAME', $mtheme_themename);
}

// Stylesheet path
$mtheme_theme_path = get_template_directory_uri();
// Theme Options Thumbnail
$mtheme_theme_icon= $mtheme_theme_path . '/images/options/thumbnail.jpg';
// Minimum contents area
if ( ! isset( $content_width ) ) { $content_width = 756; }
define('MTHEME_MIN_CONTENT_WIDTH', $content_width);
// Maximum contents area
define('MTHEME_MAX_CONTENT_WIDTH', "1040");
define('MTHEME_FULLPAGE_WIDTH', "1200");
define('MTHEME_IMAGE_QUALITY', "100");
// Max Sidebar Count
define('MTHEME_MAX_SIDEBARS', "50");
// Demo Status
define('MTHEME_DEMO_STATUS', "0");
// Theme build mode flag. Disables default enqueue font.
define('MTHEME_BUILDMODE', "0");
//Switch off Plugin scripts
define('MTHEME_PLUGIN_SCRIPT_LOAD', "0");
//Session start if demo is switched On
if (MTHEME_DEMO_STATUS) {
	if (!isset($_SESSION)) session_start();
}
// Flush permalinks on Theme Switch
function mtheme_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'mtheme_rewrite_flush' );
remove_action('init', 'mtheme_shortcode_plugin_script_style_loader');
/*-------------------------------------------------------------------------*/
/* Constants */
/*-------------------------------------------------------------------------*/
$mtheme_theme_path = get_template_directory_uri();
$mtheme_template_path=get_template_directory();
define('MTHEME_PARENTDIR', $mtheme_template_path);
define('MTHEME_FRAMEWORK', MTHEME_PARENTDIR . '/framework/' );
define('MTHEME_FRAMEWORK_PLUGINS', MTHEME_FRAMEWORK . 'plugins/' );
define('MTHEME_OPTIONS_ROOT', MTHEME_FRAMEWORK . 'options/' );
define('MTHEME_FRAMEWORK_ADMIN', MTHEME_FRAMEWORK . 'admin/' );
define('MTHEME_FRAMEWORK_FUNCTIONS', MTHEME_FRAMEWORK . 'functions/' );
define('MTHEME_FUNCTIONS', MTHEME_PARENTDIR . '/functions/' );
define('MTHEME_SHORTCODEGEN', MTHEME_FRAMEWORK . 'shortcodegen/' );
define('MTHEME_SHORTCODES', MTHEME_SHORTCODEGEN . 'shortcodes/' );
define('MTHEME_INCLUDES', MTHEME_PARENTDIR . '/includes/' );
define('MTHEME_WIDGETS', MTHEME_PARENTDIR . '/widgets/' );
define('MTHEME_IMAGES', MTHEME_PARENTDIR . '/images/' );
define('MTHEME_PATH', $mtheme_theme_path );
define('MTHEME_FONTJS', $mtheme_theme_path . '/js/font/' );

define('MTHEME_ROOT', get_template_directory_uri());
define('MTHEME_CSS', get_template_directory_uri() . '/css' );
define('MTHEME_STYLESHEET', get_stylesheet_directory_uri());
define('MTHEME_JS', get_template_directory_uri() . '/js' );

/*-------------------------------------------------------------------------*/
/* Right Click Disable
/*-------------------------------------------------------------------------*/
function mtheme_disable_rightclick() {
if ( of_get_option('rightclick_disable') ) {
	$right_block_message = stripslashes_deep( of_get_option('rightclick_disabletext') );
	$right_block_message = esc_js($right_block_message);
?> 
<script>
/* <![CDATA[ */
var message="<?php echo $right_block_message; ?>"; function clickIE4(){ if (event.button==2){ alert(message); return false; } } function clickNS4(e){ if (document.layers||document.getElementById&&!document.all){ if (e.which==2||e.which==3){ alert(message); return false; } } } if (document.layers){ document.captureEvents(Event.MOUSEDOWN); document.onmousedown=clickNS4; } else if (document.all&&!document.getElementById){ document.onmousedown=clickIE4; } document.oncontextmenu=new Function("alert(message);return false")
/* ]]> */
</script>
<?php
}
}
//add_action('wp_footer','mtheme_disable_rightclick');

/*-------------------------------------------------------------------------*/
/* Helper Variable for Javascript
/*-------------------------------------------------------------------------*/
function mtheme_uri_path_script() { 
?>
<script type="text/javascript">
var mtheme_uri="<?php echo get_template_directory_uri(); ?>";
</script>
<?php
}
add_action('wp_head', 'mtheme_uri_path_script');

/*-------------------------------------------------------------------------*/
/* Load Theme Options */
/*-------------------------------------------------------------------------*/
require_once( MTHEME_OPTIONS_ROOT .'options-caller.php');

/*-------------------------------------------------------------------------*/
/* Theme Setup */
/*-------------------------------------------------------------------------*/
require_once (MTHEME_FRAMEWORK_FUNCTIONS . 'framework-functions.php');
function mtheme_setup() {
	//Add Background Support
	add_theme_support( 'custom-background' );

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Register Menu
	register_nav_menu( 'top_menu', 'Main Menu' );
	/*-------------------------------------------------------------------------*/
	/* Internationalize for easy localizing */
	/*-------------------------------------------------------------------------*/
	load_theme_textdomain( 'mthemelocal', get_template_directory() . '/languages' );
	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) ) {
		require_once( $locale_file );
	}

	/*-------------------------------------------------------------------------*/
	/* Enable shortcodes to Text Widgets */
	/*-------------------------------------------------------------------------*/
	add_filter('widget_text', 'do_shortcode');
	/*
	 * This theme styles the visual editor to resemble the theme style and column width.
	 */
	add_editor_style( array( 'css/editor-style.css' ) );
	/*-------------------------------------------------------------------------*/
	/* Add Post Thumbnails */
	/*-------------------------------------------------------------------------*/
	add_theme_support( 'post-thumbnails' );
	// This theme supports Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio') );
	set_post_thumbnail_size( 150, 150, true ); // default thumbnail size
	add_image_size('blog-post', MTHEME_MIN_CONTENT_WIDTH, 300,true); // Blog post cropped
	add_image_size('blog-full', MTHEME_FULLPAGE_WIDTH, '',true); // Blog post images
	add_image_size('gridblock-related', 120, 64,true); // Sidebar Related image
	add_image_size('gridblock-tiny', 160, 160,true); // Sidebar Thumbnails

	add_image_size('gridblock-small', 480, 342,true); // Portfolio Small
	add_image_size('gridblock-medium', 500, 356,true); // Portfolio Medium
	add_image_size('gridblock-large', 600, 428,true); // Portfolio Large

	add_image_size('gridblock-small-portrait', 480,600,true); // Portfolio Small
	add_image_size('gridblock-medium-portrait', 500,625,true); // Portfolio Medium
	add_image_size('gridblock-large-portrait', 600,750,true); // Portfolio Large

	add_image_size('gridblock-full', MTHEME_FULLPAGE_WIDTH, '',true); // Portfolio Full
	add_image_size('gridblock-ajax', 924, '', true ); // Fullsize
	add_image_size('photowall-block', 700, '',true); // Portfolio Large
	add_image_size('admin-thumbnail', 50, '', true ); // Admin Thumbnail

	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	if ( of_get_option('rightclick_disable') ) {
		add_action( 'mtheme_contextmenu_msg', 'mtheme_contextmenu_msg_enable');
	}
}
add_action( 'after_setup_theme', 'mtheme_setup' );

/*-----------------------*/
/* Demo Panel Action 	 */
/*-----------------------*/
add_action('mtheme_demo_panel', 'mtheme_demo_panel_display');
function mtheme_demo_panel_display() {
	if ( MTHEME_DEMO_STATUS ) { 
		require ( get_template_directory() . '/framework/demopanel/demo-panel.php');
	}
}
add_action('mtheme_get_sidebar_choice', 'mtheme_sidebar_choice');
function mtheme_sidebar_choice() {
	//Get the sidebar choice
	global $mtheme_sidebar_choice,$post;
	if ( isSet($post->ID) ) {
		$mtheme_sidebar_choice= get_post_meta($post->ID, MTHEME . '_sidebar_choice', true);
	}
	$site_layout_width=of_get_option('general_theme_page');
}
add_action('mtheme_background_overlays', 'mtheme_background_overlays_display');
function mtheme_background_overlays_display() {
	if ( !wp_is_mobile() ) {
		echo '<div class="pattern-overlay"></div>';
	}
	if ( mtheme_is_fullscreen_post() ) {
		//echo '<div class="background-fill"></div>';
	}
	if ( mtheme_is_fullscreen_post() ) {
		if (post_password_required (get_the_ID()) ) {
			//echo '<div class="background-fill"></div>';
		}
	}
}
function mtheme_footer_scripts() {
	echo stripslashes_deep( of_get_option ( 'footer_scripts' ) );
}
add_action( 'wp_footer', 'mtheme_footer_scripts');

add_action('mtheme_display_password_form','mtheme_display_password_form_action');
function mtheme_display_password_form_action() {
	echo '<div id="password-protected" class="clearfix">';
	echo '<i class="fa fa-key"></i>';
	if (MTHEME_DEMO_STATUS) { echo '<p><h2>DEMO Password is 1234</h2></p>'; }
	echo get_the_password_form();
	echo '</div>';
}

add_action('mtheme_display_portfolio_single_navigation','mtheme_display_portfolio_single_navigation_action');
function mtheme_display_portfolio_single_navigation_action() {
	if (is_singular('mtheme_portfolio')) {

		$mtheme_post_arhive_link = get_post_type_archive_link( 'mtheme_portfolio' );
		$theme_options_mtheme_post_arhive_link = of_get_option('portfolio_archive_page');
		if ($theme_options_mtheme_post_arhive_link!=0) {
			$mtheme_post_arhive_link = get_page_link($theme_options_mtheme_post_arhive_link);
		}
?>
	<nav>
		<div class="portfolio-nav-wrap">
			<div class="portfolio-nav">
				<span title="<?php _e('Previous','mthemelocal'); ?>" class="portfolio-nav-item portfolio-prev">
					<?php previous_post_link_plus( array( 'order_by' => 'menu_order','format' => '%link','tooltip' => '','link' => '<i class="fa fa-chevron-left"></i>' ) ); ?>
				</span>
				<span title="<?php _e('Gallery','mthemelocal'); ?>" class="portfolio-nav-item portfolio-nav-archive">
					<a href="<?php echo $mtheme_post_arhive_link; ?>"><i class="fa fa-reorder"></i></a>
				</span>
				<span title="<?php _e('Next','mthemelocal'); ?>" class="portfolio-nav-item portfolio-next">
					<?php next_post_link_plus( array( 'order_by' => 'menu_order','format' => '%link','tooltip' => '','link' => '<i class="fa fa-chevron-right"></i>' ) ); ?>
				</span>
			</div>
		</div>
	</nav>
<?php
	}
}
// Permit eot,woff,ttf,and svg mime types for upload
add_filter('upload_mimes', 'mtheme_permit_font_uploading');
function mtheme_permit_font_uploading( $existing_mimes=array() ) {
	$existing_mimes['eot'] = 'font/eot';
	$existing_mimes['woff'] = 'font/woff';
	$existing_mimes['ttf'] = 'font/ttf';
	$existing_mimes['svg'] = 'font/svg';
	return $existing_mimes;
}
function mtheme_contextmenu_msg_enable() {
	$rightclicktext = of_get_option('rightclick_disabletext');
	echo '<div id="dimmer"><div class="dimmer-outer"><div class="dimmer-inner"><div class="dimmer-text">'.$rightclicktext.'</div></div></div></div>';
}
/*-------------------------------------------------------------------------*/
/* Enqueue Scripts
/*-------------------------------------------------------------------------*/
function mtheme_register_scripts_styles() {
		/*-------------------------------------------------------------------------*/
	/* Register Scripts and Styles
	/*-------------------------------------------------------------------------*/
	// JPlayer Script and Style
	wp_register_script( 'jPlayerJS', MTHEME_JS . '/html5player/jquery.jplayer.min.js', array( 'jquery' ),null, true );
	wp_register_style( 'css_jplayer', MTHEME_ROOT . '/css/html5player/jplayer.dark.css', array( 'MainStyle' ), false, 'screen' );

	// Touch Swipe
	wp_register_script( 'TouchSwipe', MTHEME_JS . '/jquery.touchSwipe.min.js', array( 'jquery' ),null, true );

	// Dark Theme
	wp_register_style( 'DarkStyle', MTHEME_STYLESHEET . '/style_dark.css', array( 'MainStyle' ), false, 'screen' );

	// Owl Carousel
	wp_register_script( 'owlcarousel', MTHEME_JS . '/owlcarousel/owl.carousel.min.js', array( 'jquery' ), null,true );
	wp_register_style( 'owlcarousel_css', MTHEME_ROOT . '/css/owlcarousel/owl.carousel.css', array( 'MainStyle' ), false, 'screen' );

	// Donut Chart
	wp_register_script( 'DonutChart', MTHEME_JS . '/jquery.donutchart.js', array( 'jquery' ),null, true );

	// Appear ( Unused )
	wp_register_script( 'AppearJS', MTHEME_JS . '/jquery.appear.js', array( 'jquery' ),null, true );

	// WayPoint
	wp_register_script( 'WayPointsJS', MTHEME_JS . '/waypoints/waypoints.min.js', array( 'jquery' ),null, true );

	//Backrground image strecher
	wp_register_script( 'Background_image_stretcher', MTHEME_JS . '/jquery.backstretch.min.js', array( 'jquery' ),null, true );

	// FlexSlider Script and Styles
	wp_register_script( 'flexislider', MTHEME_JS . '/flexislider/jquery.flexslider.js', array('jquery') , '',true );
	wp_register_style( 'flexislider_css', MTHEME_ROOT . '/css/flexislider/flexslider-page.css',array( 'MainStyle' ),false, 'screen' );

    // contactFormScript
    wp_register_script( 'contactform', MTHEME_JS . '/contact.js', array( 'jquery' ),null, true );

    // counter script
    wp_register_script( 'counter', MTHEME_JS . '/counter.js', array( 'jquery' ),null, true );

	if( is_ssl() ) {
		$protocol = 'https';
	} else {
		$protocol = 'http';
	}

	$googlemap_apikey=of_get_option('googlemap_apikey');
	if (!isSet($googlemap_apikey)) {
		$googlemap_apikey = '';
	}
    // Google Maps Loader
    wp_register_script( 'GoogleMaps', $protocol . '://maps.google.com/maps/api/js?key='.$googlemap_apikey, array( 'jquery' ),null, false );

    // iSotope
    wp_register_script( 'isotope', MTHEME_JS . '/jquery.isotope.min.js', array( 'jquery' ), null,true );

    // Tubular
    wp_register_script( 'tubular', MTHEME_JS . '/jquery.tubular.1.0.js', array( 'jquery' ), null,true );

	wp_enqueue_script( 'videoJS', MTHEME_JS . '/videojs/video.js', array( 'jquery' ),null, true );
	wp_enqueue_style( 'videoJSCSS', MTHEME_JS . '/videojs/video-js.css', array( 'MainStyle' ), false, 'screen' );	

	// PhotoWall INIT
    wp_register_script( 'photowall_INIT', MTHEME_JS . '/photowall.js', array( 'jquery' ), null,true );

	// Kenburns
    wp_register_script( 'kenburns_JS', MTHEME_JS . '/kenburns/jquery.slideshowify.js', array( 'jquery' ), null,true );

    wp_register_script( 'carousel_JS', MTHEME_JS . '/hcarousel.js', array( 'jquery' ), null,true );
	// Kenburns INIT
    wp_register_script( 'kenburns_INIT', MTHEME_JS . '/kenburns/kenburns.init.js', array( 'jquery' ), null,true );
	// jQTransmit
    wp_register_script( 'jQTransmit_JS', MTHEME_JS . '/kenburns/jquery.transit.min.js', array( 'jquery' ), null,true );

    // Supersized
    wp_register_script( 'supersized_JS', MTHEME_JS . '/supersized/supersized.3.2.7.min.js', array( 'jquery' ), null,true );
    wp_register_script( 'supersized_shutter_JS', MTHEME_JS . '/supersized/supersized.shutter.js', array( 'jquery' ), null,true );
    wp_register_style( 'supersized_CSS', MTHEME_CSS . '/supersized/supersized.css',array( 'MainStyle' ),false, 'screen' );

	// Mobile Menu Script
	wp_register_style( 'MobileMenuCSS', MTHEME_CSS . '/menu/mobile-menu.css',array( 'MainStyle' ),false, 'screen' );

	// Responsive Style
	wp_register_style( 'ResponsiveCSS', MTHEME_CSS . '/responsive.css',array( 'MainStyle' ),false, 'screen' );

	// Custom Style
	wp_register_style( 'CustomStyle', MTHEME_STYLESHEET . '/custom.css',array( 'MainStyle' ),false, 'screen' );

	// Dynamic Styles
	wp_register_style( 'Dynamic_CSS', MTHEME_CSS . '/dynamic_css.php',array( 'MainStyle' ),false, 'screen' );

	// Common Styles
	wp_register_script( 'superfish', MTHEME_JS . '/menu/superfish.js', array( 'jquery' ),null, true );
	wp_register_script( 'qtips', MTHEME_JS . '/jquery.tipsy.js', array( 'jquery' ),null, true );
	wp_register_script( 'magnific_lightbox', MTHEME_JS . '/magnific/jquery.magnific-popup.min.js', array( 'jquery' ),null, true );
	wp_register_script( 'twitter', MTHEME_JS . '/jquery.tweet.js', array( 'jquery' ),null, true );
	wp_register_script( 'EasingScript', MTHEME_JS . '/jquery.easing.min.js', array( 'jquery' ),null, true );
	wp_register_script( 'portfolioloader', MTHEME_JS . '/page-elements.js', array( 'jquery' ), null,true );
	wp_register_script( 'nice_scroll', MTHEME_JS . '/jquery.nicescroll.min.js', array( 'jquery' ), null,true );
	wp_register_script( 'stickymenu', MTHEME_JS . '/jquery.stickymenu.js', array( 'jquery' ), null,true );
	wp_register_script( 'fitVids', MTHEME_JS . '/jquery.fitvids.js', array( 'jquery' ), null,true );
	wp_register_script( 'excanvas', MTHEME_JS . '/excanvas.js', array( 'jquery' ),null, true );
	wp_register_script( 'ResponsiveJQIE', MTHEME_JS . '/css3-mediaqueries.js', array('jquery'),null, true );
	wp_register_script( 'custom', MTHEME_JS . '/common.js', array( 'jquery' ),null, true );

	wp_register_style( 'MainStyle', MTHEME_STYLESHEET . '/style.css',false, 'screen' );
	wp_register_style( 'fontAwesome', MTHEME_CSS . '/font-awesome/css/font-awesome.min.css', array( 'MainStyle' ), false, 'screen' );
	wp_register_style( 'magnific_lightbox', MTHEME_CSS . '/magnific/magnific-popup.css', array( 'MainStyle' ), false, 'screen' );
	wp_register_style( 'navMenuCSS', MTHEME_CSS . '/menu/superfish.css', array( 'MainStyle' ), false, 'screen' );
}
function mtheme_function_scripts_styles() {
/*-------------------------------------------------------------------------*/
/* Start Loading
/*-------------------------------------------------------------------------*/	
	/* Common Scripts */
	global $is_IE; //WordPress-specific global variable
	wp_enqueue_script('jquery');
	wp_localize_script('portfolioloader', 'ajax_var', array(
		'url' => admin_url('admin-ajax.php'),
		'nonce' => wp_create_nonce('ajax-nonce')
	));
	if($is_IE) {
		wp_enqueue_script( 'excanvas' );
	}
	wp_enqueue_script( 'superfish' );
	wp_enqueue_script( 'qtips' );
	wp_enqueue_script( 'magnific_lightbox' );
	wp_enqueue_script( 'twitter' );
	wp_enqueue_script( 'EasingScript' );
	wp_enqueue_script( 'portfolioloader' );
	wp_enqueue_script( 'nice_scroll' );
	wp_enqueue_script( 'stickymenu' );
	wp_enqueue_script( 'fitVids' );
	wp_enqueue_script( 'WayPointsJS' );
	wp_enqueue_script( 'hoverIntent' );
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-tooltip' );

	if($is_IE) {
		wp_enqueue_script( 'ResponsiveJQIE' );
	}
	wp_enqueue_script( 'custom' );

	/* Common Styles */
	wp_enqueue_style( 'MainStyle' );

	wp_enqueue_style( 'fontAwesome' );
	if ( ! MTHEME_BUILDMODE ) {
		if ( of_get_option('default_googlewebfonts')) {
		}
	}
	wp_enqueue_style( 'magnific_lightbox' );
	wp_enqueue_style( 'navMenuCSS' );
	//*** End of Common Script and Style Loads **//
	wp_enqueue_style ('MobileMenuCSS');

	// Conditional Load Flexslider
	if ( is_archive() || is_single() || is_search() || is_home() || is_page_template('template-bloglist.php') || is_page_template('template-bloglist-small.php') || is_page_template('template-bloglist_fullwidth.php') || is_page_template('template-gallery-posts.php') ) {
			wp_enqueue_script ('flexislider');
			wp_enqueue_style ('flexislider_css');
	}
	if(is_single()) {
		wp_enqueue_script ('flexislider');
		wp_enqueue_style ('flexislider_css');
		wp_enqueue_script ('TouchSwipe');
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	// Conditional Load jPlayer
	if ( is_archive() || is_single() || is_search() || is_home() || is_page_template('template-fullscreen-home.php') || is_page_template('template-bloglist.php') || is_page_template('template-bloglist-small.php') || is_page_template('template-bloglist_fullwidth.php') || is_page_template('template-video-posts.php') || is_page_template('template-audio-posts.php') ) {
			wp_enqueue_script ('jPlayerJS');
			wp_enqueue_style ('css_jplayer');
	}
	// Conditional Load Contact Form
	if ( is_page_template('template-contact.php') ) {
		wp_enqueue_script ('contactform');
	}

	if (MTHEME_DEMO_STATUS) {
		if ( isset( $_GET['demo_theme_style'] ) ) $_SESSION['demo_theme_style']=$_GET['demo_theme_style'];
		if ( isset($_SESSION['demo_theme_style'] )) $demo_theme_style = $_SESSION['demo_theme_style'];
		if ( isset($_SESSION['demo_theme_style']) && $_SESSION['demo_theme_style'] == "dark" ) {
			wp_enqueue_style ('DarkStyle');
		}
	}

	// Load Theme Dark Style
	if ( !MTHEME_DEMO_STATUS ) {
		if (of_get_option('general_theme_style')=="dark" ) {
			wp_enqueue_style ('DarkStyle');
		}
	}

	// Load Dynamic Styles last to over-ride all
	require_once ( MTHEME_PARENTDIR . '/css/dynamic_css.php' );
	wp_add_inline_style( 'ResponsiveCSS', $dynamic_css );

	if ( mtheme_is_fullscreen_post() ) {

		$featured_page=mtheme_get_active_fullscreen_post();
		
		if ( post_password_required ($featured_page) ) {
			wp_enqueue_script ('Background_image_stretcher');
		} else {

			$custom = get_post_custom( $featured_page );
			if ( isSet($custom[ MTHEME . "_fullscreen_type"][0]) ) {
				$fullscreen_type = $custom[ MTHEME . "_fullscreen_type"][0];
			}
			if (isSet($fullscreen_type)) {
				switch ($fullscreen_type) {

					case "photowall" :
						wp_enqueue_script ('Background_image_stretcher');
						wp_enqueue_script ('photowall_INIT');
						wp_enqueue_script ('isotope');
						wp_add_inline_style( 'ResponsiveCSS', "html{position:absolute;height:100%;width:100%;min-height:100%;min-width:100%;}" );
					break;

					case "kenburns" :
						wp_enqueue_script ('kenburns_JS');
						wp_enqueue_script ('jQTransmit_JS');
						wp_enqueue_script ('kenburns_INIT');
						wp_enqueue_style ('supersized_CSS');
						wp_add_inline_style( 'ResponsiveCSS', "html{position:absolute;height:100%;width:100%;min-height:100%;min-width:100%;}" );
					break;

					case "carousel" :
						wp_enqueue_script ('Background_image_stretcher');
						wp_enqueue_script ('carousel_JS');
						wp_enqueue_script ('TouchSwipe');
						wp_add_inline_style( 'ResponsiveCSS', "html{position:absolute;height:100%;width:100%;min-height:100%;min-width:100%;}" );
					break;
					
					case "slideshow" :
					case "Slideshow-plus-captions" :
						wp_enqueue_script ('supersized_JS');
						wp_enqueue_script ('supersized_shutter_JS');
						wp_enqueue_style ('supersized_CSS');
						wp_enqueue_script ('TouchSwipe');
						wp_add_inline_style( 'ResponsiveCSS', "html{position:absolute;height:100%;width:100%;min-height:100%;min-width:100%;}" );
					break;
					
					case "video" :
						if (isSet($custom[MTHEME . "_youtubevideo"][0])) {
							wp_enqueue_script ('Background_image_stretcher');
							wp_enqueue_script ('tubular');
						}
						if (isSet($custom[MTHEME . "_vimeovideo"][0])) {
							wp_add_inline_style( 'MainStyle', "body{height:1px;}" );
						}
						if ( isSet($custom[MTHEME . "_html5_mp4"][0]) || isSet($custom[MTHEME . "_html5_webm"][0]) ) {
							wp_enqueue_script('videoJS');
							wp_enqueue_style('videoJSCSS');
							wp_add_inline_style( 'ResponsiveCSS', "html{position:absolute;height:100%;width:100%;min-height:100%;min-width:100%;}" );
						}
					break;

					default:

					break;
				}
			}
		}

	}

	if ( is_singular() ) {
	// Background slideshow or image
		$bg_choice = get_post_meta( get_the_id() , MTHEME . '_meta_background_choice', true);
	}
	// Load scripts based on Background Image / Slideshow Choice
	if ( is_archive() || is_search() || is_404() ) {
		$bg_choice="default";
	}
	if ( is_home() ) {
			$bg_choice="default";
	}
	if ( mtheme_is_fullscreen_post() ) {
			$bg_choice="background_color";
	}
	if ( isSet($bg_choice) ) {
		switch ($bg_choice) { 
			case "featured_image" :
			case "custom_url" :
			case "options_image" :
				wp_enqueue_script ('Background_image_stretcher');
			break;

			case "options_slideshow" :
			case "image_attachments" :
			case "fullscreen_post" :
			//Defined in Theme framework Functions
				wp_enqueue_script ('supersized_JS');
				wp_enqueue_script ('supersized_shutter_JS');
				wp_enqueue_style ('supersized_CSS');
				wp_enqueue_script ('TouchSwipe');
			break;

			case "background_color" :
				$background_color= get_post_meta(get_the_id(), MTHEME . '_pagebackground_color', true);
				$apply_background_color = 'body { background:'.$background_color.'; }';
				wp_add_inline_style( 'ResponsiveCSS', $apply_background_color );
			break;

			default :
				wp_enqueue_script ('Background_image_stretcher');
		}
	}

	wp_enqueue_style( 'mtheme-ie', get_template_directory_uri() . '/css/ie.css', array( 'MainStyle' ), '' );

	// Embed a font Link
	if ( of_get_option('custom_font_embed')<>"" ) {
		echo stripslashes_deep( of_get_option('custom_font_embed') );
	}
	if ( of_get_option('custom_font_css')<>"" ) {
		$custom_font_css = stripslashes_deep( of_get_option('custom_font_css') );
		wp_add_inline_style( 'ResponsiveCSS', $custom_font_css );
	}

	//Start applying custom font families or fallbacks.
	$selected_css_heading_classes ='
	h1,h2,h3,h4,h5,h6,
	.menu-title,
	.header-menu-wrap li a,
	.ui-tabs-anchor,
	.donutcounter-item,
	.tp-caption,
	.divider-title,
	.item-title,
	#gridblock-filters,
	.breadcrumb,
	.social-header-wrap ul li.contact-text,
	#gridblock-filter-select,
	.skillbar,
	.time-count-data,
	.time-count-title,
	.client-position,
	.boxtitle-hover,
	.portfolio-share li.sharethis,
	.pagination-navigation,
	.mbutton,
	.sc_slideshowtitle,
	.pricing-table,
	.quote_say,
	.quote_author,
	.mtheme-dropcap,
	.woocommerce-breadcrumb,
	.mtheme-woo-order-selected-wrap,
	.woocommerce-pagination,
	.mtheme-woocommerce-description-wrap,
	.woocommerce #content button,
	.woocommerce ul.products li.product .onsale,
	.woocommerce-page ul.products li.product .onsale,
	.header-shopping-cart,
	.woocommerce table.shop_table thead,
	.woocommerce .cart .button,
	.woocommerce .price,
	#contact .button,
	.project-details h4,
	.project-client-info h4,
	.project-details-link h4 a,
	.slideshow-box-title,
	.woocommerce form .form-row label, .woocommerce-page form .form-row label,
	.tp-caption.large_bold_white,
	.tp-caption.medium_light_white,
	.wpcf7-form input[type="button"],
	.wpcf7-form input[type="submit"],
	.wpcf7-form input[type="reset"],
	li.bbp-forum-info a.bbp-forum-title,
	.bbp-forums-list a.bbp-forum-link';

	$apply_selected_css_headings = $selected_css_heading_classes . ' {
		font-family: "Raleway","Helvetica Neue",Helvetica,Arial,sans-serif;
	}';
	wp_add_inline_style( 'MainStyle', $apply_selected_css_headings );

	if( is_ssl() ) {
		$protocol = 'https';
	} else {
		$protocol = 'http';
	}
	wp_enqueue_style( 'Raleway_font', $protocol . '://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' , array( 'MainStyle' ), null, 'screen' );
	wp_enqueue_style( 'Crimson_font', $protocol . '://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600,600italic,700,700italic' , array( 'MainStyle' ), null, 'screen' );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// ******* No more styles will be loaded after this line

	// Load Fonts
	// This enqueue method through the function prevent any double loading of fonts.
	$heading_font = mtheme_enqueue_font ("heading_font");
	wp_enqueue_style( $heading_font['name'] , $heading_font['url'] , array( 'MainStyle' ), null, 'screen' );

	$page_headings = mtheme_enqueue_font ("page_headings");
	wp_enqueue_style( $page_headings['name'], $page_headings['url'] , array( 'MainStyle' ), null, 'screen' );

	$menu_font = mtheme_enqueue_font ("menu_font");
	wp_enqueue_style( $menu_font['name'], $menu_font['url'] , array( 'MainStyle' ), null, 'screen' );

	$supersized_title_font = mtheme_enqueue_font ("super_title");
	wp_enqueue_style( $supersized_title_font['name'], $supersized_title_font['url'] , array( 'MainStyle' ), null, 'screen' );

}
function mtheme_load_shortcode_scripts_styles() {
		// Conditional Load jQueries
	if(mtheme_got_shortcode('tabs') || mtheme_got_shortcode('accordion')) {
	    wp_enqueue_script('jquery-ui-core');
	    wp_enqueue_script('jquery-ui-tabs');
	    wp_enqueue_script('jquery-ui-accordion');
	}

	if(mtheme_got_shortcode('portfoliogrid') || mtheme_got_shortcode('thumbnails') || mtheme_got_shortcode('worktype_albums') || is_post_type_archive() || is_tax() ) {
		wp_enqueue_script ('isotope');
	}

	if(mtheme_got_shortcode('count')) { 
		wp_enqueue_script ('counter');
	}
	//Counter
	if(mtheme_got_shortcode('counter')) {  
		wp_enqueue_script ('DonutChart');
	}
	//Caraousel
	if(mtheme_got_shortcode('workscarousel')) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	if(mtheme_got_shortcode('woocommerce_carousel_bestselling')) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	if(mtheme_got_shortcode('map')) {
		wp_enqueue_script ('GoogleMaps');
	}

	if( mtheme_got_shortcode('woocommerce_featured_slideshow') || mtheme_got_shortcode('flexislideshow') || mtheme_got_shortcode('recent_blog_slideshow') || mtheme_got_shortcode('recent_portfolio_slideshow') || mtheme_got_shortcode('portfoliogrid') || mtheme_got_shortcode('testimonials') ) {
		wp_enqueue_script ('flexislider');
		wp_enqueue_style ('flexislider_css');
	}

	if( mtheme_got_shortcode('audioplayer')) {
		wp_enqueue_script ('jPlayerJS');
		wp_enqueue_style ('css_jplayer');
	}

	if( mtheme_got_shortcode('carousel_group') ) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
}
function mtheme_load_responsive_and_custom_styles() {
	// ******* Load Responsive and Custom Styles
	wp_enqueue_style ('ResponsiveCSS');
	wp_enqueue_style ('CustomStyle');
}
add_action( 'wp_enqueue_scripts', 'mtheme_register_scripts_styles' );
add_action( 'wp_enqueue_scripts', 'mtheme_function_scripts_styles' );
add_action( 'wp_enqueue_scripts', 'mtheme_load_shortcode_scripts_styles' );
// Last to go
add_action( 'wp_enqueue_scripts', 'mtheme_load_responsive_and_custom_styles' );

// Pagination for Custom post type singular portfoliogallery
add_filter('redirect_canonical','mtheme_disable_redirect_canonical');
function mtheme_disable_redirect_canonical( $redirect_url ) {
    if ( is_singular( 'portfoliogallery' ) )
	$redirect_url = false;
    return $redirect_url;
}

add_filter( 'option_posts_per_page', 'mtheme_tax_filter_posts_per_page' );
function mtheme_tax_filter_posts_per_page( $value ) {
    return (is_tax('types')) ? 1 : $value;
}
// Add to Body Class
function mtheme_body_class( $classes ) {
	if ( of_get_option('rightclick_disable') ) {
		$classes[] = 'rightclick-block';
	}
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( post_password_required() ) {
		$classes[] = 'mtheme-page-password-protected';
	}

	if ( mtheme_is_fullscreen_post() ) {
		$classes[] = 'page-is-fullscreen';
	}

	if ( is_page_template('template-blank.php') ) {
		$classes[] = 'page-is-blank';
	}

	$skin_style = of_get_option('general_theme_style');
	$classes[] = $skin_style;

	if ( MTHEME_DEMO_STATUS ) { 
		$classes[] = 'demo';
	}
	
	$site_layout_width=of_get_option('general_theme_page');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_SESSION['general_theme_page'] ) || isSet( $_GET['general_theme_page'] ) ) {
			if ( isSet( $_GET['general_theme_page'] ) ) $_SESSION['general_theme_page']=$_GET['general_theme_page'];
			if ( isSet($_SESSION['general_theme_page'] )) $general_theme_page = $_SESSION['general_theme_page'];
			if (isSet($general_theme_page)) {
				if ($general_theme_page==1) {
					$site_layout_width="fullwidth-theme";
				} else {
					$site_layout_width="boxed-theme";
				}
			}
		}
	}
	$classes[] = $site_layout_width;

	if ( is_singular() ) {
		$backgroundui_color= get_post_meta(get_the_id(), MTHEME . '_backgroundui_text', true);
		if ( isSet($backgroundui_color) ) {
			$classes[] = 'background-ui-'.$backgroundui_color;
		}
	}
	if ( is_archive() ) {
		$general_background_ui= of_get_option('general_background_ui');
		if ( isSet($general_background_ui) ) {
			$classes[] = 'background-ui-'.$general_background_ui;
		}
	}
	$main_header_type = of_get_option('main_header_type');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_SESSION['demo_header_type'] ) || isSet( $_GET['demo_header_type'] ) ) {
			if ( isSet( $_GET['demo_header_type'] ) ) $_SESSION['demo_header_type']=$_GET['demo_header_type'];
			if ( isSet($_SESSION['demo_header_type'] )) $demo_header_type = $_SESSION['demo_header_type'];
			if (isSet($demo_header_type)) {
				$main_header_type=$demo_header_type;
			}
		}
	}
	if (!isSet($main_header_type)) {
		$main_header_type="transparent";
	}

	$classes[] = 'header-type-'.$main_header_type;
	
	if ($main_header_type=="transparent") {
		$classes[] = 'dual-logo-support';
	}

	$classes[] = 'body-dashboard-push';

	return $classes;
}
add_filter( 'body_class', 'mtheme_body_class' );
//@ Page Menu
function mtheme_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'mtheme_page_menu_args' );
/*-------------------------------------------------------------------------*/
/* Excerpt Lenght */
/*-------------------------------------------------------------------------*/
function mtheme_excerpt_length($length) {
	return 80;
}
add_filter('excerpt_length', 'mtheme_excerpt_length');
/**
 * Creates a nicely formatted and more specific title element text for output
 */
function mtheme_wp_title( $title, $sep ) {
	global $paged, $page; //WordPress-specific global variable

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'mthemelocal' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'mtheme_wp_title', 10, 2 );
/**
 * Registers widget areas.
 */
function mtheme_widgets_init() {
	// Default Sidebar
	register_sidebar(array(
		'name' => 'Default Sidebar',
		'id' => 'default_sidebar',
		'description' => __('Default sidebar selected for pages, blog posts and archives.','mthemelocal'),
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Social Header Sidebar
	register_sidebar(array(
		'name' => 'Social Header',
		'id' => 'social_header',
		'description' => __('For social widget to display social icons.','mthemelocal'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Default Portfolio Sidebar
	register_sidebar(array(
		'name' => 'Default Portfolio Sidebar',
		'id' => 'portfolio_sidebar',
		'description' => __('Default sidebar for portfolio pages.','mthemelocal'),
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	// Dynamic Sidebar
	for ($sidebar_count=1; $sidebar_count <= MTHEME_MAX_SIDEBARS; $sidebar_count++ ) {

		if ( of_get_option('theme_sidebar'.$sidebar_count) <> "" ) {
			register_sidebar(array(
				'name' => of_get_option('theme_sidebar'.$sidebar_count),
				'description' => of_get_option('theme_sidebardesc'.$sidebar_count),
				'id' => 'sidebar_' . $sidebar_count . '',
				'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
				'after_widget' => '</aside></div>',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
			));
		}
	}
}
add_action( 'widgets_init', 'mtheme_widgets_init' );
/*-------------------------------------------------------------------------*/
/* Load Admin */
/*-------------------------------------------------------------------------*/
	require_once (MTHEME_FRAMEWORK . 'admin/admin_setup.php');
/*-------------------------------------------------------------------------*/
/* Core Libraries */
/*-------------------------------------------------------------------------*/
function mtheme_load_core_libaries() {
	require_once (MTHEME_FRAMEWORK . 'admin/tgm/class-tgm-plugin-activation.php');
	require_once (MTHEME_FRAMEWORK . 'admin/tgm/tgm-init.php');
	require_once (MTHEME_FRAMEWORK_FUNCTIONS . 'post-pagination.php');
}
/*-------------------------------------------------------------------------*/
/* Theme Specific Libraries */
/*-------------------------------------------------------------------------*/
add_action('init','mtheme_load_theme_metaboxes');
function mtheme_load_theme_metaboxes() {
	require_once (MTHEME_FRAMEWORK . 'metaboxgen/metaboxgen.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/page-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/portfolio-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/fullscreen-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/post-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/woocommerce-metaboxes.php');
}
/*-------------------------------------------------------------------------*/
/* Load Constants : Core Libraries : Update Notifier*/
/*-------------------------------------------------------------------------*/
mtheme_load_core_libaries();

if ( MTHEME_DEMO_STATUS ) { 
	require (get_template_directory() . "/framework/demopanel/demo_loader.php");
}

/* Custom ajax loader for contact form 7 */
add_filter('wpcf7_ajax_loader', 'mtheme_wpcf7_ajax_loader_icon');
function mtheme_wpcf7_ajax_loader_icon () {
	return  get_template_directory_uri() . '/images/preloaders/preloader.png';
}

// If WooCommerce Plugin is active.
if ( class_exists( 'woocommerce' ) ) {

	add_action('admin_init','mtheme_update_woocommerce_images');
	function mtheme_update_woocommerce_images() {
		global $pagenow;
		if( is_admin() && isset($_GET['activated']) && 'themes.php' == $pagenow ) {
			update_option('shop_catalog_image_size', array('width' => 400, 'height' => '', 0));
			update_option('shop_single_image_size', array('width' => 550, 'height' => '', 0));
			update_option('shop_thumbnail_image_size', array('width' => 150, 'height' => '', 0));
		}
	}

	add_theme_support( 'woocommerce' );

	add_action( 'woocommerce_before_shop_loop_item_title', 'mtheme_woocommerce_template_loop_second_product_thumbnail', 11 );
	// Display the second thumbnail on Hover
	function mtheme_woocommerce_template_loop_second_product_thumbnail() {
		global $product, $woocommerce;

		$attachment_ids = $product->get_gallery_attachment_ids();

		if ( isSet($attachment_ids) ) {
			if ( isSet($attachment_ids['0']) ) {
				$secondary_image_id = $attachment_ids['0'];
				echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array( 'class' => 'mtheme-secondary-thumbnail-image attachment-shop-catalog woo-thumbnail-fadeOutUp' ) );
			}
		}
	}

	if ( !is_admin() ) {
		add_filter( 'post_class', 'mtheme_product_has_many_images' );
	}
	// Add pif-has-gallery class to products that have a gallery
	function mtheme_product_has_many_images( $classes ) {
		global $product;

		$post_type = get_post_type( get_the_ID() );

		if ( $post_type == 'product' ) {

			$attachment_ids = $product->get_gallery_attachment_ids();
			if ( isSet($attachment_ids ) ) {
				if ( isSet($attachment_ids['0']) ) {
					$secondary_image_id = $attachment_ids['0'];
					$classes[] = 'mtheme-hover-thumbnail';
				}
			}
		}

		return $classes;
	}
	// Remove sidebars from Woocommerce generated pages
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar');

	//Remove Star rating from archives
	//remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

	add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
	function woo_custom_breadrumb_home_url() {
		return home_url().'/shop/';
	}
	function mtheme_woocommerce_category_add_to_products(){

	    $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );

	    if ( $product_cats && ! is_wp_error ( $product_cats ) ){

	        $single_cat = array_shift( $product_cats );

	        echo '<h4 itemprop="name" class="product_category_title"><span>'. $single_cat->name . '</span></h4>';

		}
	}
	add_action( 'woocommerce_single_product_summary', 'mtheme_woocommerce_category_add_to_products', 2 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'mtheme_woocommerce_category_add_to_products', 12 );

	function mtheme_remove_cart_button_from_products_arcvhive(){
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	}
	//add_action('init','mtheme_remove_cart_button_from_products_arcvhive');

	function mtheme_remove_archive_titles() {
		return false;
	}
	add_filter('woocommerce_show_page_title', 'mtheme_remove_archive_titles');

	add_action( 'wp_enqueue_scripts', 'mtheme_remove_woocommerce_styles', 99 );
	function mtheme_remove_woocommerce_styles() {
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		wp_dequeue_script( 'prettyPhoto-init' );
	}

	// Display 12 products per page.
	add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );

	// Change number or products per row to 4
	add_filter('loop_shop_columns', 'mtheme_loop_columns');
	if (!function_exists('loop_columns')) {
		function mtheme_loop_columns() {
			return 4; // 4 products per row
		}
	}

	// Remove rating from archives
	function mtheme_remove_ratings_loop(){
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
	}
	//add_action('init','mtheme_remove_ratings_loop');


}
// Will be called from slideshow script
function mtheme_populate_slide_ui_colors($get_slideshow_from_page_id) {
		// Store slide data for jQuery
	$filter_image_ids = mtheme_get_custom_attachments ( $get_slideshow_from_page_id );
	if ($filter_image_ids){
		$slide_counter=0;
		$last_slide_count = count($filter_image_ids) - 1;
		echo '<ul id="slideshow-data" data-lastslide="'.$last_slide_count.'">';
		foreach ( $filter_image_ids as $attachment_id) {
				$attachment = get_post( $attachment_id );

				$thumbnailURI = wp_get_attachment_image_src( $attachment_id, 'thumbnail', false );
				$thumbnailURI = $thumbnailURI[0];

				$thumbnail_title = apply_filters('the_title',$attachment->post_title);

				$slide_color = get_post_meta( $attachment->ID, 'mtheme_attachment_fullscreen_color', true );
				if (!$slide_color) { $slide_color="bright";}
				echo '<li class="slide-'.$slide_counter.'" data-slide="'.$slide_counter.'" data-color="'.$slide_color.'" data-thumbnail="'.$thumbnailURI.'" data-title="'.$thumbnail_title.'"></li>';
				$slide_counter++;
		}
		echo '</ul>';
	}
}
?>